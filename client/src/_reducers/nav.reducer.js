import { navConstants } from "../_constants";

let initialState = {
    navId: ""
  }

export function nav(state = initialState, action) {
  switch (action.type) {
    case navConstants.NAV_ACTIVE:
      return Object.assign(
        {},
        {
          navId: action.payload.navId
        }
      );
      break;
    case navConstants.PUSH_NAV_ACTIVE:
      return Object.assign(
        {},
        {
          pushNavActive: action.payload.pushNavActive
        }
      );

    default:
      return state;
  }
}
