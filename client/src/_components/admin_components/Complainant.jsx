import React from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import {navActions } from "../../_actions"; 
import { Card, DropdownButton, CardHeader, CardBody, ExcelExport,Layout, MainLayout } from '../../_components';
import {  tableMessages,tableSearchMessages,editColumnMessages,filterRowMessages,pagingPanelMessages,AddButton,EditButton,DeleteButton,CancelButton,CommitButton} from '../../_components/ui_components/GridUIElement';
import {
    SortingState,
    IntegratedSorting,
    SelectionState,
    PagingState,
    IntegratedPaging,
    IntegratedSelection,
    FilteringState,
    IntegratedFiltering,
    SearchState,
    EditingState,
    DataTypeProvider
} from '@devexpress/dx-react-grid';
import {
    Grid,
    Table,
    TableHeaderRow,
    TableSelection,
    PagingPanel,
    TableFilterRow,
    Toolbar,
    SearchPanel,
    TableEditRow,
    TableEditColumn,
} from '@devexpress/dx-react-grid-material-ui';

import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';

import IconButton from 'material-ui/IconButton';
import Input from 'material-ui/Input';
import Select from 'material-ui/Select';
import { TableCell } from 'material-ui/Table';
import GridRow from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';
import FileDownload from '@material-ui/icons/FileDownload';
import { withStyles } from 'material-ui/styles';
import { compliantSourceService } from '../../_services';
import { Loading } from '../../assets/devextreme_grid_theme_sources/components/loading';
import { graphql, compose, Query } from 'react-apollo';
import {COMPLAINANT_GET_ALL_QUERY,COMPLAINANT_CREATE_MUTATION,COMPLAINANT_UPDATE_MUTATION,COMPLAINANT_DELETE_MUTATION} from '../../_queries';
import { AlertList, Alert, AlertContainer } from "react-bs-notifier";
import ApolloClient from 'apollo-boost';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ReactExport from "react-data-export";
import {config} from '../../_helpers';
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
import _ from 'lodash';

const client = new ApolloClient({
    uri: config.apiUrl
});


const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    iconSmall: {
        fontSize: 20,
    },
});


const commandComponents = {
    add: AddButton,
    edit: EditButton,
    delete: DeleteButton,
    commit: CommitButton,
    cancel: CancelButton,
};

const Command = ({ id, onExecute }) => {
    const CommandButton = commandComponents[id];
    return (
        <CommandButton
            onExecute={onExecute}
        />
    );
};
const getRowId = row => row.id;

const Cell = (props) => {
    return <Table.Cell {...props} />;
};


class Complainant extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [
                { name: 'name', title: 'Şikayetçi' },
                { name: 'desc', title: 'Açıklama' },
                { name: 'relativeCreatedDate', title: 'Oluşturulma Tarihi' },
                { name: 'relativeUpdatedDate', title: 'Güncellenme Tarihi' },
            ],
            rows: [],
            sorting: [{ columnName: '', direction: '' }],
            tableColumnExtensions: [
                { columnName: 'name', width: 180 },
                { columnName: 'desc', width: 250 },
                { columnName: 'relativeCreatedDate', width: 180 },
                { columnName: 'relativeUpdatedDate', width: 180 },
            ],
            selection: [],
            selectedRows: [],
            searchValue: '',
            editingRowIds: [],
            addedRows: [],
            rowChanges: {},
            deletingRows: [],
            loading: true,
            editingStateColumnExtensions: [
                { columnName: 'relativeCreatedDate', editingEnabled: false },
                { columnName: 'relativeUpdatedDate', editingEnabled: false }
            ],
            alertPosition: "top-right",
            alerts: [],
            alertTimeout: 3000,
            alertNewMessage: "",
            anchorEl: null,
        };
        this.changeSorting = sorting => this.setState({ sorting });
        this.changeSelection = selection => {
            let that = this;
            this.setState({ selection });
            let selectedRows = [];
            selection.map((val) => {
                _.find(that.state.rows, function (row, i) {
                    if (i == val) {
                        selectedRows.push(row)
                    }
                });
            })
            this.setState({ selectedRows });

        };
        this.changeCurrentPage = currentPage => this.setState({ currentPage });
        this.changePageSize = pageSize => this.setState({ pageSize });

        this.changeSearchValue = value => this.setState({ searchValue: value });

        this.commitChanges = ({ added, changed, deleted }) => {
            let that = this;
            let { rows } = this.state;
            if (added) {
                debugger;
                const startingAddedId = rows.length > 0 ? rows[rows.length - 1].id + 1 : 0;
                let fieldData = Object.assign({}, added);
                this.createData(fieldData);
            }
            if (changed) {
                let _index = Object.keys(changed)[0];
                let row = _.find(that.state.rows, function (row, i) {
                    return i == _index
                });
                let name = changed[_index].name != undefined ? changed[_index].name : row.name,
                    desc = changed[_index].desc != undefined ? changed[_index].desc : row.desc,
                    id = row.id
                let fieldData = Object.assign({}, { name, desc, id })
                this.updateData(fieldData);
            }

            this.setState({ deletingRows: deleted || this.state.deletingRows });

        };
        this.cancelDelete = () => this.setState({ deletingRows: [] });
        this.deleteRows = () => {
            let _index = Object.keys(this.state.deletingRows)[0];
            let row = _.find(this.state.rows, function (row, i) {
                return i == _index
            });
            let id = row.id;
            this.deleteData(id);


            const rows = this.state.rows.slice();
            this.state.deletingRows.forEach((rowId) => {
                const index = rows.findIndex(row => row.id === rowId);
                if (index > -1) {
                    rows.splice(index, 1);
                }
            });
            this.setState({ rows, deletingRows: [] });
        };
    }

    componentDidMount() {
        this._mounted = true;
        this.loadData();
        this.props.dispatch(navActions.active({
            navId: "sikayetci"
          }));
    }

    componentWillUnmount() {
        this._mounted = false;
    }


    loadData() {
        let that = this;
        client.query({
            query: COMPLAINANT_GET_ALL_QUERY
        }).then(function (result) {
            that.setState({
                rows: result.data.allComplainants,
                loading: result.loading
            })
        })

    }

    createData(fieldData) {
        let that = this;
        this.props.addComplainant({
            variables: { "fields": fieldData[0] },
            refetchQueries: [{ query: COMPLAINANT_GET_ALL_QUERY }]
        }).then(function (result) {
            console.log(result);
            that.setState({ alertNewMessage: "Kaydetme işlemi başarılı!" });
            that.generateAlert("success");
        }).catch(function (error) {
            that.setState({ alertNewMessage: "Hata oluştu! Lütfen alanları kontrol ediniz." });
            that.generateAlert("danger");
        })

    }

    updateData(fieldData) {
        let that = this;
        this.props.updateComplainant({
            variables: { "fields": fieldData },
            refetchQueries: [{ query: COMPLAINANT_GET_ALL_QUERY }]
        }).then(function (result) {
            console.log(result);
            that.setState({ alertNewMessage: "Güncelleme işlemi başarılı!" });
            that.generateAlert("success");
        }).catch(function (error) {
            that.setState({ alertNewMessage: "Hata oluştu! Lütfen alanları kontrol ediniz." });
            that.generateAlert("danger");
        })

    }

    deleteData(id) {
        let that = this;
        this.props.deleteComplainant({
            variables: { "id": id },
            refetchQueries: [{ query: COMPLAINANT_GET_ALL_QUERY }]
        }).then(function (result) {
            console.log(result);
            that.setState({ alertNewMessage: "Kayıt Silme işlemi başarılı!" });
            that.generateAlert("success");
        }).catch(function (error) {
            that.setState({ alertNewMessage: "Hata oluştu! Lütfen alanları kontrol ediniz." });
            that.generateAlert("danger");
        })

    }

    generateAlert(type) {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: "",
            message: this.state.alertNewMessage
        };

        this.setState({
            alerts: [...this.state.alerts, newAlert]
        });
    }

    onAlertDismissed(alert) {
        const alerts = this.state.alerts;

        // find the index of the alert that was dismissed
        const idx = alerts.indexOf(alert);

        if (idx >= 0) {
            this.setState({
                // remove the alert from the array
                alerts: [...alerts.slice(0, idx), ...alerts.slice(idx + 1)]
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getAllData) {
            if (nextProps.getAllData.allComplainants) {
                this.setState({
                    rows: nextProps.getAllData.allComplainants
                })
            }
        }
    }

    handleExportClick(event) {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleExportClose() {
        this.setState({ anchorEl: null });
    };


    render() {

        const {
            rows,
            columns,
            tableColumnExtensions,
            sorting,
            selection,
            selectedRows,
            pageSize,
            pageSizes,
            currentPage,
            searchValue,
            editingRowIds,
            deletingRows,
            addedRows,
            rowChanges,
            loading,
            dateColumns,
            editingStateColumnExtensions,
            anchorEl
        } = this.state;

        const { classes } = this.props;

        return (
            <MainLayout>
                <Card iconName="view_list" >
                    <CardHeader title="Şikayetçi Yönetimi" iconName="list" headerIcon headerRose></CardHeader>
                    <CardBody>
                        <AlertList
                            position={this.state.alertPosition}
                            alerts={this.state.alerts}
                            timeout={this.state.alertTimeout}
                            dismissTitle="Begone!"
                            onDismiss={this.onAlertDismissed.bind(this)}
                        />
                        <GridRow>
                            <GridRow item xs={3}>
                                <Button
                                    aria-owns={anchorEl ? 'simple-menu' : null}
                                    aria-haspopup="true"
                                    onClick={this.handleExportClick.bind(this)}
                                    variant="outlined"
                                    color="primary"
                                >
                                    Excele Aktar
                                <FileDownload className={classes.rightIcon} />
                                </Button>
                                <Menu
                                    id="simple-menu"
                                    anchorEl={anchorEl}
                                    open={Boolean(anchorEl)}
                                    onClose={this.handleExportClose.bind(this)}
                                >
                                    <MenuItem onClick={this.handleExportClose.bind(this)}>
                                        <ExcelFile element={<span>Tüm Kayıtları Aktar</span>}>
                                            <ExcelSheet data={rows} name="Şikayet Kaynakları">
                                                <ExcelColumn label="Kaynak Adı" value="name" />
                                                <ExcelColumn label="Açıklama" value="desc" />
                                                <ExcelColumn label="Oluşturulma Tarihi" value="relativeCreatedDate" />
                                                <ExcelColumn label="Güncellenme Tarihi" value="relativeUpdatedDate" />
                                            </ExcelSheet>
                                        </ExcelFile>
                                    </MenuItem>
                                    <MenuItem onClick={this.handleExportClose.bind(this)}>
                                        <ExcelFile element={<span>Seçili Olan Kayıtları Aktar</span>}>
                                            <ExcelSheet data={selectedRows} name="Şikayet Kaynakları">
                                                <ExcelColumn label="Kaynak Adı" value="name" />
                                                <ExcelColumn label="Açıklama" value="desc" />
                                                <ExcelColumn label="Oluşturulma Tarihi" value="relativeCreatedDate" />
                                                <ExcelColumn label="Güncellenme Tarihi" value="relativeUpdatedDate" />
                                            </ExcelSheet>
                                        </ExcelFile>
                                    </MenuItem>
                                </Menu>

                            </GridRow>
                        </GridRow>
                        <Grid rows={rows} columns={columns} >
                            <EditingState
                                editingRowIds={editingRowIds}
                                onEditingRowIdsChange={this.changeEditingRowIds}
                                rowChanges={rowChanges}
                                onRowChangesChange={this.changeRowChanges}
                                addedRows={addedRows}
                                onAddedRowsChange={this.changeAddedRows}
                                onCommitChanges={this.commitChanges}
                                defaultEditingRowIds={[0]}
                                columnExtensions={editingStateColumnExtensions}
                            />
                            <SortingState
                                sorting={sorting}
                                onSortingChange={this.changeSorting}
                            />
                            <PagingState
                                currentPage={currentPage}
                                onCurrentPageChange={this.changeCurrentPage}
                                pageSize={pageSize}
                                onPageSizeChange={this.changePageSize}
                            />

                            <SearchState
                                value={searchValue}
                                onValueChange={this.changeSearchValue}
                            />
                            <IntegratedPaging />
                            <IntegratedSorting />
                            <IntegratedFiltering />
                            <SelectionState
                                selection={selection}
                                onSelectionChange={this.changeSelection}
                            />
                            <Table
                                columnExtensions={tableColumnExtensions}
                                cellComponent={Cell}
                                messages={tableMessages} />
                            <TableHeaderRow showSortingControls />
                            <PagingPanel
                                pageSizes={pageSizes}
                                messages={pagingPanelMessages} />
                            <Toolbar />
                            <SearchPanel messages={tableSearchMessages} />
                            <TableEditRow />
                            <TableEditColumn
                                width={120}
                                showAddCommand={!addedRows.length}
                                showEditCommand
                                showDeleteCommand
                                commandComponent={Command}
                                messages={editColumnMessages}
                            />
                            <TableSelection />
                        </Grid>
                        {loading && <Loading />}
                        {<Dialog
                            open={!!deletingRows.length}
                            onClose={this.cancelDelete}
                        >
                            <DialogTitle>Kayıt Silme İşlemi</DialogTitle>
                            <DialogContent>
                                <DialogContentText>Bu kaydı silmek istediğinize emin misiniz?</DialogContentText>
                                <Grid rows={rows.filter((row, i) => { return i == Object.keys(deletingRows)[0] })} columns={columns}>
                                    <Table
                                        columnExtensions={tableColumnExtensions}
                                        cellComponent={Cell}
                                    />
                                    <TableHeaderRow />
                                </Grid>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={this.cancelDelete} color="primary">İptal</Button>
                                <Button onClick={this.deleteRows} color="secondary">Sil</Button>
                            </DialogActions>
                        </Dialog>}
                    </CardBody>
                </Card>
            </MainLayout>
        );
    }
}


Complainant.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const {navId} = state.nav;
    return {
      navId
    };
  }

const connectedComplainant = connect(mapStateToProps)(Complainant);  

const composedComplainant = compose(
    graphql(COMPLAINANT_GET_ALL_QUERY, { name: "getAllData" }),
    graphql(COMPLAINANT_CREATE_MUTATION, { name: "addComplainant" }),
    graphql(COMPLAINANT_UPDATE_MUTATION, { name: "updateComplainant" }),
    graphql(COMPLAINANT_DELETE_MUTATION, { name: "deleteComplainant" })
)(withStyles(styles)(connectedComplainant));

export {composedComplainant as Complainant}


