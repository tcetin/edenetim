import React from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import {navActions } from "../../_actions"; 
import { Card, DropdownButton, CardHeader, CardBody, ExcelExport, MainLayout } from '../../_components';
import {  tableMessages,tableSearchMessages,editColumnMessages,filterRowMessages,pagingPanelMessages} from '../../_components/ui_components/GridUIElement';
import {
    SortingState,
    IntegratedSorting,
    SelectionState,
    PagingState,
    IntegratedPaging,
    IntegratedSelection,
    FilteringState,
    IntegratedFiltering,
    SearchState,
    EditingState,
    DataTypeProvider
} from '@devexpress/dx-react-grid';
import {
    Grid,
    Table,
    TableHeaderRow,
    TableSelection,
    PagingPanel,
    TableFilterRow,
    Toolbar,
    SearchPanel,
    TableEditRow,
    TableEditColumn,
} from '@devexpress/dx-react-grid-material-ui';

import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import Icon from '@material-ui/core/Icon';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from 'material-ui/IconButton';
import Input from 'material-ui/Input';
import Select from 'material-ui/Select';
import { TableCell } from 'material-ui/Table';
import GridRow from '@material-ui/core/Grid';
import FileDownload from '@material-ui/icons/FileDownload';
import CreateNew from '@material-ui/icons/CreateNewFolder';
import { withStyles } from 'material-ui/styles';
import { compliantSourceService } from '../../_services';
import { Loading } from '../../assets/devextreme_grid_theme_sources/components/loading';
import { graphql, compose, Query } from 'react-apollo';
import {USER_GET_ALL_QUERY,USER_CREATE_MUTATION,USER_UPDATE_MUTATION,USER_DELETE_MUTATION} from '../../_queries';
import { AlertList, Alert, AlertContainer } from "react-bs-notifier";
import ApolloClient from 'apollo-boost';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ReactExport from "react-data-export";
import {config} from '../../_helpers';
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
import _ from 'lodash';
import Drawer from '@material-ui/core/Drawer';
import AppBar from 'material-ui/AppBar';
import swal from 'sweetalert2';


const client = new ApolloClient({
    uri: config.apiUrl
});


const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    iconSmall: {
        fontSize: 20,
    },
});


const getRowId = row => row.id;

const Cell = (props) => {
    return <Table.Cell {...props} />;
};


class User extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [
                { name: 'username', title: 'Kullanıcı Adı(TC)' },
                { name: 'firstName', title: 'Ad' },
                { name: 'lastName', title: 'Soyad' },
                { name: 'email', title: 'Eposta' },
                { name: 'phone', title: 'Telefon' },
                { name: 'relativeCreatedDate', title: 'Oluşturulma Tarihi' },
                { name: 'relativeUpdatedDate', title: 'Güncellenme Tarihi' },
            ],
            rows: [],
            sorting: [{ columnName: '', direction: '' }],
            tableColumnExtensions: [
                { columnName: 'username', width: 180 },
                { columnName: 'firstName', width: 180 },
                { columnName: 'lastName', width: 180 },
                { columnName: 'email', width: 180 },
                { columnName: 'phone', width: 180 },
                { columnName: 'relativeCreatedDate', width: 180 },
                { columnName: 'relativeUpdatedDate', width: 180 },
            ],
            selection: [],
            selectedRows: [],
            searchValue: '',
            editingRowIds: [],
            addedRows: [],
            rowChanges: {},
            deletingRows: [],
            loading: true,
            editingStateColumnExtensions: [
                { columnName: 'relativeCreatedDate', editingEnabled: false },
                { columnName: 'relativeUpdatedDate', editingEnabled: false }
            ],
            alertPosition: "top-right",
            alerts: [],
            alertTimeout: 3000,
            alertNewMessage: "",
            anchorEl: null,
            dialogOpen:false,
            username:'',
            firstName:'',
            lastName:'',
            email:'',
            phone:'',
            formIsValid:false,
            usernameError:false,
            firstNameError:false,
            lastNameError:false,
            emailError:false,
            phoneError:false,
            formId: 0,
            formType:''//add-update
        };
        this.changeSorting = sorting => this.setState({ sorting });
        this.changeSelection = selection => {
            let that = this;
            this.setState({ selection });
            let selectedRows = [];
            selection.map((val) => {
                _.find(that.state.rows, function (row, i) {
                    if (i == val) {
                        selectedRows.push(row)
                    }
                });
            })
            this.setState({ selectedRows });

        };
        this.changeCurrentPage = currentPage => this.setState({ currentPage });
        this.changePageSize = pageSize => this.setState({ pageSize });

        this.changeSearchValue = value => this.setState({ searchValue: value });

        this.commitChanges = ({ added, changed, deleted }) => {
            let that = this;
            let { rows } = this.state;
            if (added) {
                const startingAddedId = rows.length > 0 ? rows[rows.length - 1].id + 1 : 0;
                let fieldData = Object.assign({}, added);
                this.createData(fieldData);
            }
            if (changed) {
                let _index = Object.keys(changed)[0];
                let row = _.find(that.state.rows, function (row, i) {
                    return i == _index
                });
                let username = changed[_index].username != undefined ? changed[_index].username : row.username,
                    firstName = changed[_index].firstName!= undefined ? changed[_index].firstName: row.firstName,
                    lastName = changed[_index].lastName != undefined ? changed[_index].lastName : row.lastName,
                    email = changed[_index].email != undefined ? changed[_index].email : row.email,
                    phone = changed[_index].phone != undefined ? changed[_index].phone : row.phone,
                    id = row.id
                let fieldData = Object.assign({}, { firstName,username,lastName,email,phone })
                this.updateData(fieldData);
            }

            this.setState({ deletingRows: deleted || this.state.deletingRows });

        };
        this.cancelDelete = () => this.setState({ deletingRows: [] });
        this.deleteRows = () => {
            this.deleteData();
            const rows = this.state.rows.slice();
            this.state.deletingRows.forEach((rowId) => {
                const index = rows.findIndex(row => row.id === rowId);
                if (index > -1) {
                    rows.splice(index, 1);
                }
            });
            this.setState({ rows, deletingRows: [] });
        };

        this.handleDialogClickOpen = this.handleDialogClickOpen.bind(this);

    }

    componentDidMount() {
        this.loadData();
        this.props.dispatch(navActions.active({
            navId: "kullanicilar"
          }));
    }

    loadData() {
        let that = this;
        client.query({
            query: USER_GET_ALL_QUERY
        }).then(function (result) {
            that.setState({
                rows: result.data.allUsers,
                loading: result.loading
            })
        })

    }

    deleteData() {
        let that = this;
        this.props.deleteUser({
            variables: { "id": that.state.formId },
            refetchQueries: [{ query: USER_GET_ALL_QUERY }]
        }).then(function (result) {
            console.log(result);
            that.setState({ alertNewMessage: "Kayıt Silme işlemi başarılı!" });
            that.generateAlert("success");
        }).catch(function (error) {
            that.setState({ alertNewMessage: "Hata oluştu! Lütfen alanları kontrol ediniz." });
            that.generateAlert("danger");
        })

        this.clearSelection()
    }

    generateAlert(type) {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: "",
            message: this.state.alertNewMessage
        };

        this.setState({
            alerts: [...this.state.alerts, newAlert]
        });
    }

    onAlertDismissed(alert) {
        const alerts = this.state.alerts;

        // find the index of the alert that was dismissed
        const idx = alerts.indexOf(alert);

        if (idx >= 0) {
            this.setState({
                // remove the alert from the array
                alerts: [...alerts.slice(0, idx), ...alerts.slice(idx + 1)]
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.getAllData) {
            if (nextProps.getAllData.allUsers) {
                this.setState({
                    rows: nextProps.getAllData.allUsers
                })
            }
        }
    }

    handleExportClick(event) {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleExportClose() {
        this.setState({ anchorEl: null });
    }

    handleDialogClickOpen(event) {
        let formType = event.currentTarget.id
        let dialogOpen = true;
        if(formType=='edit'){
            if(this.state.selectedRows.length>1){
                swal({
                    type: 'warning',
                    title: '',
                    text: 'Güncelleme işlemi için sadece bir kayıt seçilebilir.',
                    confirmButtonText:'Tamam'
                  })
                  
                this.setState({
                    selection:[]
                });

                 dialogOpen=false;
            }else{
                const {selectedRows} = this.state;
                this.setState({
                    formId:selectedRows[0].id,
                    username:selectedRows[0].username,
                    firstName:selectedRows[0].firstName,
                    lastName:selectedRows[0].lastName,
                    email:selectedRows[0].email,
                    phone:selectedRows[0].phone
                });
            }
        }

        
        this.setState({ dialogOpen,formType});

    }

    handleDialogClose() {
        this.setState({
            dialogOpen: false,
            username:'',
            firstName:'',
            lastName:'',
            email:'',
            phone:'',
            usernameError:false,
            firstNameError:false,
            lastNameError:false,
            emailError:false,
            phoneError:false,
        });
    }

    formHandleChange(event){
        let _name = event.target.id;
        let _value = event.target.value;
        let _formError = _name.concat('Error'); 

        this.setState({
            [_name]: _value,
            [_formError]: !_value
        })
    }

    createData(){
        let that = this;
        const {username,firstName,lastName,email,phone} = this.state;
        this.props.addUser({
            variables: { "fields": { username,firstName,lastName,email,phone } },
            refetchQueries: [{ query: USER_GET_ALL_QUERY }]
        }).then(function (result) {
            console.log(result);
            that.setState({ alertNewMessage: "Kaydetme işlemi başarılı!" });
            that.generateAlert("success");
        }).catch(function (error) {
            that.setState({ alertNewMessage: "Hata oluştu! Lütfen alanları kontrol ediniz." });
            that.generateAlert("danger");
        })
    }

    updateData(){
        let that = this;
        const { formId, username,firstName,lastName,email,phone } = this.state;
        this.props.updateUser({
            variables: { "fields": { id: formId, username,firstName,lastName,email,phone } },
            refetchQueries: [{ query: USER_GET_ALL_QUERY }]
        }).then(function (result) {
            console.log(result);
            that.setState({ alertNewMessage: "Güncelleme işlemi başarılı!" });
            that.generateAlert("success");
        }).catch(function (error) {
            that.setState({ alertNewMessage: "Hata oluştu! Lütfen alanları kontrol ediniz." });
            that.generateAlert("danger");
        })
    }

    clearSelection(){
        this.setState({
            selection:[],
            selectedRows:[]
        });
    }

    deleteOperation(){
        if (this.state.selectedRows.length > 1) {
            swal({
                type: 'warning',
                title: '',
                text: 'Silme işlemi için sadece bir kayıt seçilebilir.',
                confirmButtonText: 'Tamam'
            })
            this.clearSelection()
        } else {
            const { selectedRows,selection } = this.state;
            let deletingRow = [selection[0]];
            this.setState({
                formId: selectedRows[0].id,
                deletingRows: deletingRow
            });
        }
        
    }

    createOrUpdateData(){
        switch (this.state.formType) {
            case "add":
                this.createData();
                break;
            case "edit":
                this.updateData();
                this.clearSelection()
                break;
            default:
                break;
        }
        this.handleDialogClose()
    }

    render() {

        const {
            rows,
            columns,
            tableColumnExtensions,
            sorting,
            selection,
            selectedRows,
            pageSize,
            pageSizes,
            currentPage,
            searchValue,
            editingRowIds,
            deletingRows,
            addedRows,
            rowChanges,
            loading,
            dateColumns,
            editingStateColumnExtensions,
            anchorEl,
            dialogOpen,
            username,
            firstName,
            lastName,
            email,
            phone,
            formIsValid,
            usernameError,
            firstNameError,
            lastNameError,
            emailError,
            phoneError,
            formType
        } = this.state;

        let editdeleteBtnDisabled = selectedRows.length == 0

        const { classes } = this.props;

        return (
            <MainLayout>
            <Card iconName="view_list" >
                <CardHeader title="Kullanıcı Yönetimi" iconName="list" headerIcon headerRose></CardHeader>
                <CardBody>
                    <AlertList
                        position={this.state.alertPosition}
                        alerts={this.state.alerts}
                        timeout={this.state.alertTimeout}
                        dismissTitle="Begone!"
                        onDismiss={this.onAlertDismissed.bind(this)}
                    />
                    <Dialog open={dialogOpen} aria-labelledby="form-dialog-title">
                        <DialogTitle id="form-dialog-title">{formType=='add'?'Yeni Kullanıcı':formType=='edit'?'Kullanıcı Güncelle':''}</DialogTitle>
                        <DialogContent>
                        <DialogContentText><small className="text-danger">(*) işareti olan alanlar zorunludur.</small></DialogContentText>
                            <TextField required helperText={usernameError?'Kullanıcı Adı Alanı Zorunludur!':''} error={usernameError} autoFocus margin="dense" id="username" label="Kullanıcı Adı(TC)" fullWidth value={username} onChange={this.formHandleChange.bind(this)}/>
                            <TextField required helperText={firstNameError?'Ad Alanı Zorunludur!':''} error={firstNameError}  margin="dense" id="firstName" label="Ad" fullWidth value={firstName} onChange={this.formHandleChange.bind(this)}/>
                            <TextField required helperText={lastNameError?'Soyad Alanı Zorunludur!':''} error={lastNameError}  margin="dense" id="lastName" label="Soyad" fullWidth value={lastName} onChange={this.formHandleChange.bind(this)}/>
                            <TextField required helperText={emailError?'Eposta Alanı Zorunludur!':''} error={emailError}  margin="dense" id="email" label="Eposta" fullWidth value={email} onChange={this.formHandleChange.bind(this)}/>
                            <TextField required helperText={phoneError?'Telefon Alanı Zorunludur!':''} error={phoneError}  margin="dense" id="phone" label="Telefon" fullWidth value={phone} onChange={this.formHandleChange.bind(this)}/>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleDialogClose.bind(this)} color="primary">İptal</Button>
                            <Button onClick={this.createOrUpdateData.bind(this)} color="primary" disabled={(!username || !firstName || !lastName || !email || !phone)}>{formType=='add'?'Kaydet':formType=='edit'?'Güncelle':''}</Button>
                        </DialogActions>
                    </Dialog>
                    <GridRow>
                        <GridRow container spacing={16}>
                            <Button variant="outlined" color="primary" id="add" aria-label="add" className={classes.button} onClick={this.handleDialogClickOpen}>
                                <AddIcon />
                            </Button>
                            <Button variant="outlined" disabled={editdeleteBtnDisabled} color="secondary" id="edit" aria-label="edit" className={classes.button} onClick={this.handleDialogClickOpen}>
                                <Icon>edit_icon</Icon>
                            </Button>
                            <Button variant="outlined" disabled={editdeleteBtnDisabled} aria-label="delete" id="delete" className={classes.button} onClick={this.deleteOperation.bind(this)}>
                                <DeleteIcon />
                            </Button>
                            <Button
                                aria-owns={anchorEl ? 'simple-menu' : null}
                                aria-haspopup="true"
                                onClick={this.handleExportClick.bind(this)}
                                variant="outlined"
                                color="primary"
                                className={classes.button}
                                disabled={!rows.length}
                            >
                             <Icon>cloud_download</Icon>
                            </Button>
                            <Menu
                                id="simple-menu"
                                anchorEl={anchorEl}
                                open={Boolean(anchorEl)}
                                onClose={this.handleExportClose.bind(this)}
                            >
                                    <MenuItem onClick={this.handleExportClose.bind(this)}>
                                        <ExcelFile element={<span>Tüm Kayıtları Aktar</span>}>
                                            <ExcelSheet data={rows} name="Kullanıcılar">
                                                <ExcelColumn label="Kullanıcı Adı(TC)" value="username" />
                                                <ExcelColumn label="Ad" value="firstName" />
                                                <ExcelColumn label="Soyad" value="lastName" />
                                                <ExcelColumn label="Eposta" value="email" />
                                                <ExcelColumn label="Telefon" value="phone" />
                                                <ExcelColumn label="Oluşturulma Tarihi" value="relativeCreatedDate" />
                                                <ExcelColumn label="Güncellenme Tarihi" value="relativeUpdatedDate" />
                                            </ExcelSheet>
                                        </ExcelFile>
                                    </MenuItem>
                                    <MenuItem onClick={this.handleExportClose.bind(this)}>
                                        <ExcelFile  element={<span>Seçili Olan Kayıtları Aktar</span>}>
                                            <ExcelSheet data={selectedRows} name="Kullanıcılar">
                                                <ExcelColumn label="Kullanıcı Adı(TC)" value="username" />
                                                <ExcelColumn label="Ad" value="firstName" />
                                                <ExcelColumn label="Soyad" value="lastName" />
                                                <ExcelColumn label="Eposta" value="email" />
                                                <ExcelColumn label="Telefon" value="phone" />
                                                <ExcelColumn label="Oluşturulma Tarihi" value="relativeCreatedDate" />
                                                <ExcelColumn label="Güncellenme Tarihi" value="relativeUpdatedDate" />
                                            </ExcelSheet>
                                        </ExcelFile>
                                    </MenuItem>
                            </Menu>

                        </GridRow>
                    </GridRow>  
                    <Grid rows={rows} columns={columns} >
                        <EditingState
                            editingRowIds={editingRowIds}
                            onEditingRowIdsChange={this.changeEditingRowIds}
                            rowChanges={rowChanges}
                            onRowChangesChange={this.changeRowChanges}
                            addedRows={addedRows}
                            onAddedRowsChange={this.changeAddedRows}
                            onCommitChanges={this.commitChanges}
                            defaultEditingRowIds={[0]}
                            columnExtensions={editingStateColumnExtensions}
                        />
                        <SortingState
                            sorting={sorting}
                            onSortingChange={this.changeSorting}
                        />
                        <PagingState
                            currentPage={currentPage}
                            onCurrentPageChange={this.changeCurrentPage}
                            pageSize={pageSize}
                            onPageSizeChange={this.changePageSize}
                        />

                        <SearchState
                            value={searchValue}
                            onValueChange={this.changeSearchValue}
                        />
                        <IntegratedPaging />
                        <IntegratedSorting />
                        <IntegratedFiltering />
                        <SelectionState
                            selection={selection}
                            onSelectionChange={this.changeSelection}
                        />
                        <Table
                            columnExtensions={tableColumnExtensions}
                            cellComponent={Cell}
                            messages={tableMessages} />
                        <TableHeaderRow showSortingControls />
                        <PagingPanel
                            pageSizes={pageSizes}
                            messages={pagingPanelMessages} />
                        <Toolbar />
                        <SearchPanel messages={tableSearchMessages} />
                        <TableEditRow />
                        <TableEditColumn
                            width={120}
                            messages={editColumnMessages}
                        />
                        <TableSelection />
                    </Grid>
                    {loading && <Loading />}
                    {<Dialog
                        open={!!deletingRows.length}
                        onClose={this.cancelDelete}
                    >
                        <DialogTitle>Kayıt Silme İşlemi</DialogTitle>
                        <DialogContent>
                            <DialogContentText>Bu kaydı silmek istediğinize emin misiniz?</DialogContentText>
                            <Grid rows={rows.filter((row, i) => { return i == deletingRows[0] })} columns={columns}>
                                <Table
                                    columnExtensions={tableColumnExtensions}
                                    cellComponent={Cell}
                                />
                                <TableHeaderRow />
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.cancelDelete} color="primary">İptal</Button>
                            <Button onClick={this.deleteRows} color="secondary">Sil</Button>
                        </DialogActions>
                    </Dialog>}
                </CardBody>
            </Card>
            </MainLayout>
        );
    }
}


User.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const {navId} = state.nav;
    return {
      navId
    };
  }

const connectedUser = connect(mapStateToProps)(User);  

const composedUser = compose(
    graphql(USER_GET_ALL_QUERY, { name: "getAllData" }),
    graphql(USER_CREATE_MUTATION, { name: "addUser" }),
    graphql(USER_UPDATE_MUTATION, { name: "updateUser" }),
    graphql(USER_DELETE_MUTATION, { name: "deleteUser" })
)(withStyles(styles)(connectedUser));

export {composedUser as User}


