import React from 'react';
import {ADMIN_REPORT_GET_QUERY} from '../../_queries';
import {connect} from 'react-redux';
import {navActions } from "../../_actions"; 
import { graphql,Query } from 'react-apollo';
import {MainLayout} from '../../_components';
import { Loading } from '../../assets/devextreme_grid_theme_sources/components/loading';

export const AdminReport = () => (
    <Query query={ADMIN_REPORT_GET_QUERY}>
        {({ loading, error, data }) => {
            if (loading) return <Loading />;
            if (error) return `Error! ${error.message}`;

            const {adminReport} = data; 

            return (
                <MainLayout>
                <div className="row">
                    <div className="col-lg-3">
                        <div className="card card-stats">
                            <div className="card-header card-header-rose card-header-icon">
                                <div className="card-icon">
                                    <i className="material-icons">equalizer</i>
                                </div>
                                <p className="card-category">Şikayetçi Türü Sayısı</p>
                                <h3 className="card-title">{adminReport.complaintCount}</h3>
                            </div>
                            <div className="card-footer"></div>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="card card-stats">
                            <div className="card-header card-header-rose card-header-icon">
                                <div className="card-icon">
                                    <i className="material-icons">equalizer</i>
                                </div>
                                <p className="card-category">Şikayet Kaynağı Sayısı</p>
                                <h3 className="card-title">{adminReport.complaintSourceCount}</h3>
                            </div>
                            <div className="card-footer"></div>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="card card-stats">
                            <div className="card-header card-header-rose card-header-icon">
                                <div className="card-icon">
                                    <i className="material-icons">equalizer</i>
                                </div>
                                <p className="card-category">Konu Kodu Sayısı</p>
                                <h3 className="card-title">{adminReport.subjectParentCount}</h3>
                            </div>
                            <div className="card-footer"></div>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="card card-stats">
                            <div className="card-header card-header-rose card-header-icon">
                                <div className="card-icon">
                                    <i className="material-icons">equalizer</i>
                                </div>
                                <p className="card-category">Görev Türü Sayısı</p>
                                <h3 className="card-title">{adminReport.taskTypeCount}</h3>
                            </div>
                            <div className="card-footer"></div>
                        </div>
                    </div>
                </div>
                </MainLayout>
            );
        }}
    </Query>
)


// function mapStateToProps(state) {
//     const {navId} = state.nav;
//     return {
//       navId
//     };
//   }

// const connectedReport = connect(mapStateToProps)(AdminReport);  

// const composedReport = compose(
//     graphql(ADMIN_REPORT_GET_QUERY, { name: "getAllData" })
// )(connectedReport);

// export {composedReport as AdminReport}