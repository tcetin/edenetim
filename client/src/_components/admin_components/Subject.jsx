import * as React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import {navActions } from "../../_actions"; 
import { Card, DropdownButton, CardHeader, CardBody, ExcelExport,SelectWrapped,MainLayout } from '../../_components';
import Paper from 'material-ui/Paper';
import {
    DataTypeProvider,
    TreeDataState, SortingState, SelectionState, FilteringState, PagingState,
    CustomTreeData, IntegratedFiltering, IntegratedPaging, IntegratedSorting, IntegratedSelection,
} from '@devexpress/dx-react-grid';
import {
    Grid,
    Table, TableHeaderRow, TableFilterRow, TableTreeColumn,
    PagingPanel, TableColumnResizing, Toolbar, TableColumnVisibility, ColumnChooser,
} from '@devexpress/dx-react-grid-material-ui';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';
import { Loading } from '../../assets/devextreme_grid_theme_sources/components/loading';
import {filterRowMessages,pagingPanelMessages} from '../../_components/ui_components/GridUIElement';
import {SUBJECT_GET_ALL_QUERY,SUBJECT_CREATE_MUTATION,SUBJECT_UPDATE_MUTATION,SUBJECT_DELETE_MUTATION} from '../../_queries';
import { AlertList, Alert, AlertContainer } from "react-bs-notifier";
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from 'material-ui/styles';
import GridRow from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';
import DeleteIcon from '@material-ui/icons/Delete';
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
import ReactExport from "react-data-export";

import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import Typography from '@material-ui/core/Typography';
import ClearIcon from '@material-ui/icons/Clear';
import CancelIcon from '@material-ui/icons/Cancel';

import Select from 'react-select';
import 'react-select/dist/react-select.css';
import Chip from '@material-ui/core/Chip';
import Input from '@material-ui/core/Input';
import swal from 'sweetalert2';

import {config} from '../../_helpers';
import _ from 'lodash';
import ApolloClient from 'apollo-boost';
import { graphql, compose, Query } from 'react-apollo';

const client = new ApolloClient({
    uri: config.apiUrl
});

  
  const ITEM_HEIGHT = 48;
  
  const styles = theme => ({
    root: {
      //flexGrow: 1,
    },
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    iconSmall: {
        fontSize: 20,
    },
    '@global': {
      '.Select-control': {
        display: 'flex',
        alignItems: 'center',
        border: 0,
        height: 'auto',
        background: 'transparent',
        '&:hover': {
          boxShadow: 'none',
        },
      },
      '.Select-multi-value-wrapper': {
        flexGrow: 1,
        display: 'flex',
        flexWrap: 'wrap',
      },
      '.Select--multi .Select-input': {
        margin: 0,
      },
      '.Select.has-value.is-clearable.Select--single > .Select-control .Select-value': {
        padding: 0,
      },
      '.Select-noresults': {
        padding: theme.spacing.unit * 2,
      },
      '.Select-input': {
        display: 'inline-flex !important',
        padding: 0,
        height: 'auto',
      },
      '.Select-input input': {
        background: 'transparent',
        border: 0,
        padding: 0,
        cursor: 'default',
        display: 'inline-block',
        fontFamily: 'inherit',
        fontSize: 'inherit',
        margin: 0,
        outline: 0,
      },
      '.Select-placeholder, .Select--single .Select-value': {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        display: 'flex',
        alignItems: 'center',
        fontFamily: theme.typography.fontFamily,
        fontSize: theme.typography.pxToRem(16),
        padding: 0,
      },
      '.Select-placeholder': {
        opacity: 0.42,
        color: theme.palette.common.black,
      },
      '.Select-menu-outer': {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[2],
        position: 'absolute',
        left: 0,
        top: `calc(100% + ${theme.spacing.unit}px)`,
        width: '100%',
        zIndex: 2,
        maxHeight: ITEM_HEIGHT * 4.5,
      },
      '.Select.is-focused:not(.is-open) > .Select-control': {
        boxShadow: 'none',
      },
      '.Select-menu': {
        maxHeight: ITEM_HEIGHT * 4.5,
        overflowY: 'auto',
      },
      '.Select-menu div': {
        boxSizing: 'content-box',
      },
      '.Select-arrow-zone, .Select-clear-zone': {
        color: theme.palette.action.active,
        cursor: 'pointer',
        height: 21,
        width: 21,
        zIndex: 1,
      },
      // Only for screen readers. We can't use display none.
      '.Select-aria-only': {
        position: 'absolute',
        overflow: 'hidden',
        clip: 'rect(0 0 0 0)',
        height: 1,
        width: 1,
        margin: -1,
      },
    },
  });
  

const Cell = (props) => {
    return <Table.Cell {...props} />;
};

 class Subject extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            columns: [
                { name: 'code', title: 'Konu Kodu' },
                { name: 'name', title: 'Konu Adı' },
                { name: 'desc', title: 'Açıklama' },
                { name: 'relativeCreatedDate', title: 'Oluşturulma Zamanı' },
                { name: 'relativeUpdatedDate', title: 'Güncellenme Zamanı' }
            ],
            rows: [],
            pageSizes: [5, 10, 20],
            defaultColumnWidths: [
                { columnName: 'code', width: 180 },
                { columnName: 'name', width: 180 },
                { columnName: 'desc', width: 300 },
                { columnName: 'relativeCreatedDate', width: 180 },
                { columnName: 'relativeUpdatedDate', width: 180 }
            ],
            alertPosition: "top-right",
            alerts: [],
            alertTimeout: 3000,
            alertNewMessage: "",
            dialogOpen:false,
            formId: 0,
            formType:'',
            formNameError:false,
            formCodeError:false,
            formCode:'',
            formName:'',
            formDesc:'',
            anchorEl: null,
            selection: [],
            selectedRows: [],
            deletingRows: [],
            selectedParentSubject:0,
            loading: true,
        };

        this.changeSelection = selection => {
            let that = this;
            this.setState({ selection });
            let selectedRows = [];
            selection.map((val) => {
                _.find(that.state.rows, function (row, i) {
                    if (i == val) {
                        selectedRows.push(row)
                    }
                });
            })
            this.setState({ selectedRows });

        };

        this.deleteRows = () => {
            this.deleteData();
            const rows = this.state.rows.slice();
            this.state.deletingRows.forEach((rowId) => {
                const index = rows.findIndex(row => row.id === rowId);
                if (index > -1) {
                    rows.splice(index, 1);
                }
            });
            this.setState({ rows, deletingRows: [] });
        };

        this.cancelDelete = () => this.setState({ deletingRows: [] });
    }

    componentDidMount(){
        this.loadData();
        this.props.dispatch(navActions.active({
            navId: "konu_kodu_yonetimi"
          }));
    }


    loadData() {
        let that = this;
        client.query({
            query: SUBJECT_GET_ALL_QUERY
        }).then(function (result) {
            that.setState({
                rows: result.data.allSubjects,
                loading: result.loading
            })
        })

    }

    generateAlert(type) {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: "",
            message: this.state.alertNewMessage
        };

        this.setState({
            alerts: [...this.state.alerts, newAlert]
        });
    }

    onAlertDismissed(alert) {
        const alerts = this.state.alerts;

        // find the index of the alert that was dismissed
        const idx = alerts.indexOf(alert);

        if (idx >= 0) {
            this.setState({
                // remove the alert from the array
                alerts: [...alerts.slice(0, idx), ...alerts.slice(idx + 1)]
            });
        }
    }

    handleExportClick(event) {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleExportClose() {
        this.setState({ anchorEl: null });
    }

    
    handleDialogClose() {
        this.setState({
            dialogOpen: false,
            formType:'',
            formNameError:false,
            formCodeError:false,
            formCode:'',
            formName:'',
            formDesc:'',
            selectedParentSubject:0
        });
    }

    handleDialogClickOpen(event) {
        let formType = event.currentTarget.id
        let dialogOpen = true;
        if(formType=='edit'){
            if(this.state.selectedRows.length>1){
                swal({
                    type: 'warning',
                    title: '',
                    text: 'Güncelleme işlemi için sadece bir kayıt seçilebilir.',
                    confirmButtonText:'Tamam'
                  })
                  
                this.setState({
                    selection:[]
                });

                 dialogOpen=false;
            }else{
                const {selectedRows} = this.state;
                this.setState({
                    formId:selectedRows[0].id,
                    selectedParentSubject:selectedRows[0].parentId,
                    formName:selectedRows[0].name,
                    formCode:selectedRows[0].code,
                    formDesc:selectedRows[0].desc
                });
            }
        }

        
        this.setState({ dialogOpen,formType});

    }

    formHandleChange(event){
        let _name = event.target.id;
        let _value = event.target.value;
        let _formError = _name.concat('Error'); 

        this.setState({
            [_name]: _value,
            [_formError]: !_value
        })
    }

    createData(){
        let that = this;
        this.props.addSubject({
            variables: { "fields": { parentId:that.state.selectedParentSubject,code: that.state.formCode, name: that.state.formName, desc: that.state.formDesc } },
            refetchQueries: [{ query: SUBJECT_GET_ALL_QUERY }]
        }).then(function (result) {
            console.log(result);
            that.setState({ alertNewMessage: "Kaydetme işlemi başarılı!" });
            that.generateAlert("success");
        }).catch(function (error) {
            that.setState({ alertNewMessage: "Hata oluştu! Lütfen alanları kontrol ediniz." });
            that.generateAlert("danger");
        })
    }

    updateData(){
        let that = this;
        this.props.updateSubject({
            variables: { "fields": { id:that.state.formId,parentId:that.state.selectedParentSubject,code: that.state.formCode, name: that.state.formName, desc: that.state.formDesc } },
            refetchQueries: [{ query: SUBJECT_GET_ALL_QUERY }]
        }).then(function (result) {
            console.log(result);
            that.setState({ alertNewMessage: "Kaydetme işlemi başarılı!" });
            that.generateAlert("success");
        }).catch(function (error) {
            that.setState({ alertNewMessage: "Hata oluştu! Lütfen alanları kontrol ediniz." });
            that.generateAlert("danger");
        })
    }

    createOrUpdateData(){
        switch (this.state.formType) {
            case "add":
                this.createData();
                break;
            case "edit":
                this.updateData();
                this.clearSelection()
                break;
            default:
                break;
        }
        this.handleDialogClose()
    }

    deleteOperation(){
        if (this.state.selectedRows.length > 1) { 
            swal({
                type: 'warning',
                title: '',
                text: 'Silme işlemi için sadece bir kayıt seçilebilir.',
                confirmButtonText: 'Tamam'
            })
            this.clearSelection()
        } else {
            const { selectedRows,selection } = this.state;
            let deletingRow = [selection[0]];
            this.setState({
                formId: selectedRows[0].id,
                deletingRows: deletingRow
            });
        }
        
    }

    deleteData() {
        let that = this;
        this.props.deleteSubject({
            variables: { "id": that.state.formId },
            refetchQueries: [{ query: SUBJECT_GET_ALL_QUERY }]
        }).then(function (result) {
            console.log(result);
            that.setState({ alertNewMessage: "Kayıt Silme işlemi başarılı!" });
            that.generateAlert("success");
        }).catch(function (error) {
            that.setState({ alertNewMessage: "Hata oluştu! Lütfen alanları kontrol ediniz." });
            that.generateAlert("danger");
        })

        this.clearSelection()
    }

    handleSelectChange(value){
        this.setState({
            selectedParentSubject: value,
        });
      };
    
    clearSelection(){
        this.setState({
            selection:[],
            selectedRows:[]
        });
    }

    handleExportData(data){
       const exportData = data.map(row=>({
            code:row.code,
            parentCode:row.parent?row.parent.code:'',
            name:row.name,
            desc:row.desc,
        }));
        return exportData;
    }

    getChildRows(row, rows){
        const childRows = rows.filter(r => r.parentId === (row ? row.id : 0));
        return childRows.length ? childRows : null;
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.getAllData) {
            if (nextProps.getAllData.allSubjects) {
                this.setState({
                    rows: nextProps.getAllData.allSubjects
                })
            }
        }
    }

    render() {
        const {
            rows, 
            columns, 
            pageSizes, 
            defaultColumnWidths,
            dialogOpen,
            anchorEl,
            selection,
            selectedRows,
            selectedParentSubject,
            formNameError,
            formCodeError,
            formType,
            formCode,
            formName,
            formDesc,
            loading,
            deletingRows
        } = this.state;

        let editdeleteBtnDisabled = selectedRows.length == 0

        const { classes } = this.props;

        const selectOptions = rows.map(row => ({
            value: row.id,
            label: row.code,
          }));

        return (
            <MainLayout>
            <Card iconName="view_list" >
                <CardHeader title="Konu Kodu Yönetimi" iconName="list" headerIcon headerRose></CardHeader>
                <CardBody>
                    <AlertList
                        position={this.state.alertPosition}
                        alerts={this.state.alerts}
                        timeout={this.state.alertTimeout}
                        dismissTitle="Begone!"
                        onDismiss={this.onAlertDismissed.bind(this)}
                    />
                    <Dialog open={dialogOpen} aria-labelledby="form-dialog-title">
                        <DialogTitle id="form-dialog-title">{formType=='add'?'Yeni Konu Kodu':formType=='edit'?'Konu Kodu Güncelle':''}</DialogTitle>
                        <DialogContent>
                        <DialogContentText><small className="text-danger">(*) işareti olan alanlar zorunludur.</small></DialogContentText>
                            <Input
                                fullWidth
                                inputComponent={SelectWrapped}
                                value={selectedParentSubject}
                                onChange={this.handleSelectChange.bind(this)}
                                placeholder="Ana Konu Kodu"
                                id="react-select-single"
                                inputProps={{
                                    classes,
                                    name: 'react-select-single',
                                    instanceId: 'react-select-single',
                                    simpleValue: true,
                                    options: selectOptions,
                                }}
                            />
                            <TextField required helperText={formCodeError?'Konu Kodu Alanı Zorunludur!':''} error={formCodeError} autoFocus margin="dense" id="formCode" label="Konu Kodu" fullWidth value={formCode} onChange={this.formHandleChange.bind(this)}/>
                            <TextField required helperText={formNameError?'Konu Adı Alanı Zorunludur!':''} error={formNameError}  margin="dense" id="formName" label="Konu Adı" fullWidth value={formName} onChange={this.formHandleChange.bind(this)}/>
                            <TextField margin="dense" id="formDesc" label="Konu Açıklaması" multiline rows="4" fullWidth value={formDesc} onChange={this.formHandleChange.bind(this)}/>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleDialogClose.bind(this)} color="primary">İptal</Button>
                            <Button onClick={this.createOrUpdateData.bind(this)} color="primary" disabled={(!formName || !formCode)}>{formType=='add'?'Kaydet':formType=='edit'?'Güncelle':''}</Button>
                        </DialogActions>
                    </Dialog>
                    <GridRow>
                        <GridRow container spacing={16}>
                            <Button variant="outlined" color="primary" id="add" aria-label="add" className={classes.button} onClick={this.handleDialogClickOpen.bind(this)}>
                                <AddIcon />
                            </Button>
                            <Button variant="outlined" disabled={editdeleteBtnDisabled} color="secondary" id="edit" aria-label="edit" className={classes.button} onClick={this.handleDialogClickOpen.bind(this)}>
                                <Icon>edit_icon</Icon>
                            </Button>
                            <Button variant="outlined" disabled={editdeleteBtnDisabled} aria-label="delete" id="delete" className={classes.button} onClick={this.deleteOperation.bind(this)}>
                                <DeleteIcon />
                            </Button>
                            <Button
                                aria-owns={anchorEl ? 'simple-menu' : null}
                                aria-haspopup="true"
                                onClick={this.handleExportClick.bind(this)}
                                variant="outlined"
                                color="primary"
                                className={classes.button}
                                disabled={!rows.length}
                            >
                             <Icon>cloud_download</Icon>
                            </Button>
                            <Menu
                                id="simple-menu"
                                anchorEl={anchorEl}
                                open={Boolean(anchorEl)}
                                onClose={this.handleExportClose.bind(this)}
                            >
                                    <MenuItem onClick={this.handleExportClose.bind(this)}>
                                        <ExcelFile element={<span>Tüm Kayıtları Aktar</span>}>
                                            <ExcelSheet data={this.handleExportData(rows)} name="Konu Kodları">
                                                <ExcelColumn label="Konu Kodu" value="code" />
                                                <ExcelColumn label="Ana Konu Kodu" value="parentCode" />
                                                <ExcelColumn label="Konu Adı" value="name" />
                                                <ExcelColumn label="Açıklama" value="desc" />
                                            </ExcelSheet>
                                        </ExcelFile>
                                    </MenuItem>
                                    <MenuItem onClick={this.handleExportClose.bind(this)}>
                                        <ExcelFile  element={<span>Seçili Olan Kayıtları Aktar</span>}>
                                            <ExcelSheet data={this.handleExportData(selectedRows)} name="Konu Kodları">
                                            <ExcelColumn label="Konu Kodu" value="code" />
                                                <ExcelColumn label="Ana Konu Kodu" value="parentCode" />
                                                <ExcelColumn label="Konu Adı" value="name" />
                                                <ExcelColumn label="Açıklama" value="desc" />
                                            </ExcelSheet>
                                        </ExcelFile>
                                    </MenuItem>
                            </Menu>

                        </GridRow>
                    </GridRow>  
                    <Grid
                        rows={rows}
                        columns={columns}
                    >
                        <TreeDataState />
                        <FilteringState/>
                        <SortingState />
                        <SelectionState 
                            selection={selection}
                            onSelectionChange={this.changeSelection}/>
                        <PagingState
                            defaultCurrentPage={0}
                            defaultPageSize={pageSizes[1]}
                        />

                        <CustomTreeData
                            getChildRows={this.getChildRows}
                        />
                        <IntegratedFiltering />
                        <IntegratedSelection />
                        <IntegratedSorting />
                        <IntegratedPaging />

                        <Table />
                        <TableHeaderRow
                            showSortingControls
                        />
                        <TableFilterRow messages={filterRowMessages}/>
                        <TableTreeColumn
                            for="code"
                            showSelectionControls
                            showSelectAll
                        />

                        <Toolbar />
                        <PagingPanel
                            pageSizes={pageSizes}
                            messages={pagingPanelMessages}
                        />
                    </Grid>
                    {loading && <Loading />}
                    {<Dialog
                        open={!!deletingRows.length}
                        onClose={this.cancelDelete}
                    >
                        <DialogTitle>Kayıt Silme İşlemi</DialogTitle>
                        <DialogContent>
                            <DialogContentText>Bu kaydı silmek istediğinize emin misiniz?</DialogContentText>
                            <Grid rows={rows.filter((row, i) => { return i == deletingRows[0] })} columns={columns}>
                                <Table
                                    columnExtensions={defaultColumnWidths}
                                    cellComponent={Cell}
                                />
                                <TableHeaderRow />
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.cancelDelete} color="primary">İptal</Button>
                            <Button onClick={this.deleteRows} color="secondary">Sil</Button>
                        </DialogActions>
                    </Dialog>}
                </CardBody>
            </Card>
            </MainLayout>
        );
    }
}

Subject.propTypes = {
    classes: PropTypes.object.isRequired,
};


function mapStateToProps(state) {
    const {navId} = state.nav;
    return {
      navId
    };
  }

const connectedSubject = connect(mapStateToProps)(Subject);  

const composedSubject = compose(
    graphql(SUBJECT_GET_ALL_QUERY, { name: "getAllData" }),
    graphql(SUBJECT_CREATE_MUTATION, { name: "addSubject" }),
    graphql(SUBJECT_UPDATE_MUTATION, { name: "updateSubject" }),
    graphql(SUBJECT_DELETE_MUTATION, { name: "deleteSubject" })
)(withStyles(styles)(connectedSubject));

export {composedSubject as Subject}