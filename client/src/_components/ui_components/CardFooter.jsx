import React from "react";
import { connect } from "react-redux";
import classNames from 'classnames';

export const CardFooter = props => {
  return (
      <div className="card-footer text-right">{props.children}</div>
  )
}
