import React from "react";
import { connect } from "react-redux";
import {navActions } from "../../_actions"; 
import Nav from "./Nav";
import NavItem from './NavItem';
import classNames from 'classnames';
import uuid from 'uuid';
import {Profile} from '../../_components';

class Sidebar extends React.Component {
    constructor(props) {
        super(props);
      }

      componentDidMount(){
        let that = this;
        this.props.dispatch(navActions.active({
            navId: "anasayfa"
          }));
      }

      render(){
          return(
            <div className="sidebar" data-color="danger" data-image="dist/assets/img/saglik-bakanligi-logo.png">
            <div className="logo">
            </div>
            <div className="sidebar-wrapper">
              <Profile imgSrc="dist/assets/img/default-avatar.png"/>
              <Nav>
                 <NavItem navText="Anasayfa" navIcon="home" linkTo="/" itemId="anasayfa"/>
                 <NavItem navText="Şikayet Kaynakları" navIcon="dashboard" linkTo="/sikayet_kaynaklari" itemId="sikayet_kaynaklari"/>
                 <NavItem navText="Şikayetçi Türleri" navIcon="dashboard" linkTo="/sikayetci" itemId="sikayetci"/>
                 <NavItem navText="Görev Türleri" navIcon="dashboard" linkTo="/gorev_tur" itemId="gorev_tur"/>
                 <NavItem navText="Konu Kodları" navIcon="dashboard" linkTo="/konu_kodlari" itemId="konu_kodu_yonetimi"/>
                 <NavItem navText="Kullanıcılar" navIcon="people" linkTo="/kullanicilar" itemId="kullanicilar"/>
              </Nav>
            </div>
          </div>
          );

      }

}

function mapStateToProps(state) {
  const {navId} = state.nav;
  return {
    navId
  };
}


const connectedSidebar = connect(mapStateToProps)(Sidebar);

export { connectedSidebar as Sidebar };