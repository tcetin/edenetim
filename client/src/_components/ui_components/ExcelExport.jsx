import React from 'react';
import ReactExport from "react-data-export";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;


class ExcelExport extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            columns:[],
            data:[]
        }
    }

    componentDidMount(){
        const {columns,data} = this.props
        this.setState({
            columns,
            data
        });
        debugger;
    }


    render() {
        const {element,name} = this.props
        const {columns,data} = this.state

        return (
            <ExcelFile  element={element}>
                <ExcelSheet data={data} name={name}>
                    {columns.map(function(col){
                        <ExcelColumn label={col.title} value={col.name} key={col.name}/>
                    })}
                </ExcelSheet>
            </ExcelFile>
        );
    }
}

const ExcelExportComponent = (ExcelExport);

export {ExcelExportComponent as ExcelExport};