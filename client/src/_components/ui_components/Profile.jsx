import React from 'react';
import classNames from 'classnames';
import { navActions } from "../../_actions";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class Profile extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            collapse: ['collapse'],
            ariaExpanded: false,
            user: JSON.parse(localStorage.getItem('user'))
        }

    }

    toggleCollapse() {
        let collapse = ['collapse'];
        let ariaExpanded = false;
        if (this.state.collapse.indexOf('show') === -1) {
            collapse.push('show');
            ariaExpanded = true;
        }

        this.setState({ collapse: [...collapse], ariaExpanded })
    }

    handleMenuItemClick(event) {
        this.props.dispatch(navActions.active({
            navId: event.target.href
        }));
    }


    render() {
        const { collapse, ariaExpanded, user } = this.state;
        return (
            <div className="user">
                <div className="photo">
                    <img src={this.props.imgSrc} />
                </div>
                <div className="user-info">
                    <a data-toggle="collapse" style={{ cursor: 'pointer' }} className={ariaExpanded ? '' : 'username'} aria-expanded={ariaExpanded} onClick={this.toggleCollapse.bind(this)}><span>{user.firstName}{" "}{user.lastName}<b className="caret"></b></span></a>
                    <div className={classNames(collapse)}>
                        <ul className="nav">
                            <li className="nav-item">
                                <Link to="/kullanici_profili" className="nav-link" onClick={this.handleMenuItemClick.bind(this)}>
                                    <span className="sidebar-mini"><i className="material-icons">person</i></span>
                                    <span className="sidebar-normal"> Kullanıcı Profili </span>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/giris" className="nav-link" onClick={this.handleMenuItemClick.bind(this)}>
                                    <span className="sidebar-mini"><i className="material-icons">input</i></span>
                                    <span className="sidebar-normal"> Çıkış Yap </span>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { navId } = state.nav;
    return {
        navId
    };
}

const connectedProfile = connect(mapStateToProps)(Profile);

export { connectedProfile as Profile }