import React from "react";
import { connect } from "react-redux";
import classNames from 'classnames';

export const CardHeader = props => {
  let headerClasses = ['card-header'];
  if(props.headerTab){
    headerClasses.push('card-header-tabs')
  }

  if(props.headerRose){
    headerClasses.push('card-header-rose')
  }

  if(props.headerIcon){
    headerClasses.push('card-header-icon');
  }

  return (
      <div className={classNames(headerClasses)}>
        <div className="card-icon">
          <i className="material-icons">{props.iconName}</i>
        </div>
        
        <h4 className="card-title">{props.title}</h4>
        {props.children}
      </div>
  )
}