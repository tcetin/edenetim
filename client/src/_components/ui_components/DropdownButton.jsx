import React from 'react';
import classnames from 'classnames';

export const DropdownButton = props => {
    let btnClass = ['dropdown-toggle btn btn-block']

    let icon = ""
    
    if(props.info){
        btnClass.push("btn-info");
    }

    if(props.danger){
        btnClass.push("btn-danger");
    }

    if(props.warning){
        btnClass.push("btn-warning");
    }

    if(props.success){
        btnClass.push("btn-success");
    }

    if(props.icon){
        icon = <i className="material-icons">{props.iconName}</i>
    }


    return ( 
        <div className="dropdown">
            <button className={classnames(btnClass)} type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {icon}{" "}
            {props.title}
            <div className="ripple-container"></div></button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style={{ position: 'absolute', top: '40px', left: '1px', willChange: 'top, left' }}>
                <h6 className="dropdown-header">{props.headerTitle}</h6>
                {props.children}
            </div>
        </div>)
}