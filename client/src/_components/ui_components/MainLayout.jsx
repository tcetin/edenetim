import React from 'react';
import { connect } from "react-redux";
import {Sidebar} from '../../_components';
import Avatar from '@material-ui/core/Avatar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    row: {
      display: 'flex',
      justifyContent: 'center',
    },
    avatar: {
      margin: 10,
    }
  };

const MainLayout = props => {
    
    const { classes } = props;

    return(
    <div className="wrapper">
        <div className="main-panel">
            <nav className="navbar navbar-expand-lg navbar-transparent  navbar-absolute fixed-top">
                <div className="container-fluid">
                    <div className="navbar-wrapper">
                        <div className="navbar-minimize">
                            <button id="minimizeSidebar" className="btn btn-just-icon btn-white btn-fab btn-round">
                                <i className="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                                <i className="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                            </button>
                        </div>
                        <a className="navbar-brand" href="/">T.C. Sağlık Bakanlığı E-Denetim Uygulaması</a>
                    </div>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="navbar-toggler-icon icon-bar" />
                        <span className="navbar-toggler-icon icon-bar" />
                        <span className="navbar-toggler-icon icon-bar" />
                    </button>
                </div>
            </nav>
            <div className="content">
                <div className="container-fluid">
                    <Sidebar/>
                    {props.children}
                </div>
            </div>
        </div>
    </div>)

}

MainLayout.propTypes = {
    classes: PropTypes.object.isRequired,
  };

  const styledLayout = withStyles(styles)(MainLayout)

  export {styledLayout as MainLayout}