import React from 'react';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import Button from '@material-ui/core/Button';
import IconButton from 'material-ui/IconButton';

const tableMessages = {
  noData: "Herhangi bir veri bulunamadı!"
};

const tableSearchMessages = {
  searchPlaceholder: "Ara..."
};

const editColumnMessages = {
  addCommand: "Yeni"
};

const filterRowMessages = {
  filterPlaceholder: "Filtre..."
};
const pagingPanelMessages = {
  showAll: "Tüm Kayıt",
  rowsPerPage: "Sayfa Başına Satır Sayısı",
  info: "{from}-{to} ({count})"
};

const AddButton = ({ onExecute }) => (
  <div style={{ textAlign: 'center' }}>
      <Button
          color="primary"
          onClick={onExecute}
          title="Create new row"
      >
          Yeni Kayıt
    </Button>
  </div>
);

const EditButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Edit row">
      <EditIcon />
  </IconButton>
);

const DeleteButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Delete row">
      <DeleteIcon />
  </IconButton>
);

const CommitButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Save changes">
      <SaveIcon />
  </IconButton>
);

const CancelButton = ({ onExecute }) => (
  <IconButton color="secondary" onClick={onExecute} title="Cancel changes">
      <CancelIcon />
  </IconButton>
);

export {
  tableMessages,
  tableSearchMessages,
  editColumnMessages,
  filterRowMessages,
  pagingPanelMessages,
  AddButton,
  EditButton,
  DeleteButton,
  CancelButton,
  CommitButton
}