import React from "react";
import { connect } from "react-redux";
import { mapActions } from "../../_actions";

export default class Nav extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <ul className="nav">
                {this.props.children}
            </ul>
        );
    }
}

// function mapStateToProps(state) {
//     const { hospitalType} = state.map;
//     return {
//       hospitalType
//     };
// }


// const connectedHospitalMapNav= connect(mapStateToProps)(Nav);
// export { connectedHospitalNav as Nav };