import React from "react";
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import uuid from 'uuid';
import { mapActions,navActions } from "../../_actions";
import { Link } from "react-router-dom";
import classNames from 'classnames';

class NavItem extends React.Component{
    constructor(props){
        super(props);

        this.PropTypes = {
            hasSubNav:PropTypes.bool.isRequired,
            navMainText:PropTypes.string,
            navText:PropTypes.string,
            mainNavIcon:PropTypes.string,
            navIcon:PropTypes.string,
            itemId:PropTypes.string.isRequired,
            linkTo:PropTypes.string
        }

        this.state = {
            isActive:false,
            updateComponent:false
        }
    }

    handleMenuItemClick(hospitalType){

        let that = this;

          this.props.dispatch(navActions.active({
            navId: that.props.itemId
          }));

      }

    componentDidMount(){
        if (this.props.itemId == this.props.navId) {
          this.setState({ isActive: true });
        }
    }

    componentWillReceiveProps(nextProps){
        if (this.props.itemId == nextProps.navId) {
            this.setState({ isActive: true });
          }else{
            this.setState({ isActive: false });
          }
    }

    render(){

        let subNavId = uuid(); 

        let navElement = this.props.hasSubNav ? 

            <li className="nav-item">
                <a className="nav-link" data-toggle="collapse" href={'#'+subNavId} aria-expanded="false">
                    <i className="material-icons">{this.props.mainNavIcon}</i>
                    <p>{this.props.navMainText}<b className="caret" /></p>
                </a>
                <div className="collapse" id={subNavId}><ul className="nav">{this.props.children}</ul></div>
            </li>:    
            <li className={classNames("nav-item", {active: this.state.isActive})} onClick={() => this.handleMenuItemClick(this.props.mapType)}>
                <Link to={this.props.linkTo} className="nav-link">
                    <span className="sidebar-mini">
                        <i className="material-icons">{this.props.navIcon}</i>
                    </span>
                    <span className="sidebar-normal">
                        {this.props.navText}
                    </span>
                </Link>
            </li>;

        return(
            navElement
        );
    }
}


function mapStateToProps(state) {
    const {navId} = state.nav;
    return {
      navId
    };
}


const connectedNavItem = connect(mapStateToProps)(NavItem);

export default connectedNavItem;
