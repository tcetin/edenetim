import React from "react";
import { connect } from "react-redux";
import classNames from 'classnames';
export const Card = props => {
  return (
    <div className="card">
      {props.children}
    </div> 
  )
}

