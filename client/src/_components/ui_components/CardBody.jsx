import React from "react";
import { connect } from "react-redux";
import classNames from 'classnames';

export const CardBody = props => {
  return (
      <div className="card-body" style={{minHeight:'300px'}}>{props.children}</div>
  )
}
