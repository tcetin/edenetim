import React from "react";
import { Card,GridComponent } from '../_components';
import { Query,Mutation,graphql } from "react-apollo";
import gql from "graphql-tag";
import { Grid, Table, TableHeaderRow } from '@devexpress/dx-react-grid-material-ui';

const GET_ALL_QUERY = gql`
{ allComplaintSourceTypes {
        id
        name
        desc
        relativeCreatedDate
        relativeUpdatedDate
    }
}`;

const CREATE_MUTATION = gql`
mutation ($fields: CompliantSourceInputType!) {
    createCompliantSource(compliantSource: $fields) {
      id
      name
      desc
      relativeCreatedDate
      relativeUpdatedDate
    }
  }`;

const gridColumns = [
    { name: 'name', title: 'Kaynak Adı' },
    { name: 'desc', title: 'Kaynak Açıklaması' },
    { name: 'relativeCreatedDate', title: 'Oluşturulma Tarihi' },
    { name: 'relativeUpdatedDate', title: 'Güncellenme Tarihi' }, 
];

const editingStateColumnExtensions = [
    { columnName: 'relativeCreatedDate', editingEnabled: false },
    { columnName: 'relativeUpdatedDate', editingEnabled: false }
];

class GridTest extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {

        return (
                <Query query={GET_ALL_QUERY}>
                    {({ loading, error, data }) => {
                        if (loading) return <p>Loading...</p>;
                        if (error) return <p>Error :(</p>;      
                        return (
                            <GridComponent 
                                // rows={data.allComplaintSourceTypes} 
                                columns={gridColumns} 
                                title="Şikayet Kaynağı Yönetimi"
                                editingStateColumnExtensions = {editingStateColumnExtensions}
                                getDataQuery={GET_ALL_QUERY}
                                createQuery={CREATE_MUTATION}/>
                        );
                    }}
                </Query>
        )
    }
}

const grid = GridTest;
export {grid as GridTest};