import React from "react";
import { Card, GridComponent } from '../_components';
import { Query, Mutation, graphql,compose } from "react-apollo";
import gql from "graphql-tag";
import ApolloClient from 'apollo-boost';

const client = new ApolloClient({
    uri: "http://localhost:5000/graphql"
});

const GET_ALL_QUERY = gql`
{ allComplaintSourceTypes {
        id
        name
        desc
        relativeCreatedDate
        relativeUpdatedDate
    }
}`;

const CREATE_MUTATION = gql`
mutation ($fields: CompliantSourceInputType!) {
    createCompliantSource(compliantSource: $fields) {
      id
      name
      desc
      relativeCreatedDate
      relativeUpdatedDate
    }
  }`;

class TestComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            name:'',
            desc:''
        }

    }

    componentDidMount() {
        this.loadData();
    }

    loadData() {
        let that = this;
        client.query({
            query: GET_ALL_QUERY
        }).then(function (result) {
            that.setState({
                rows: result.data.allComplaintSourceTypes
            })
        })
    }

    displayAuthors(){
        var data = this.props.getQuery;
        if(data.loading){
            return( <option disabled>Loading authors</option> );
        } else {
            return data.allComplaintSourceTypes.map(_data => {
                return( <li key={ _data.id }>{ _data.name }</li> );
            });
        }
    }
       

    submitForm(e){
        let that = this;
        e.preventDefault()
        // use the addBookMutation
        this.props.addComplaint({
            variables: {
                "fields":{
                    "name":this.state.name,
                    "desc":this.state.desc
                }
            },
            refetchQueries: [{ query: GET_ALL_QUERY }]
        });
    }



    render() {
        return (
            <div>
                <ul>
                    {this.displayAuthors()}
                </ul>
            <form id="add-book" onSubmit={ this.submitForm.bind(this) } >
                <div className="field">
                    <label>Name:</label>
                    <input type="text" onChange={ (e) => this.setState({ name: e.target.value }) } />
                </div>
                <div className="field">
                    <label>Desc:</label>
                    <input type="text" onChange={ (e) => this.setState({ desc: e.target.value }) } />
                </div>
                <button>+</button>
                </form>
            </div>
        )
    }
}

const test = compose(
    graphql(GET_ALL_QUERY, { name: "getQuery" }),
    graphql(CREATE_MUTATION, { name: "addComplaint" })
)(TestComponent)
export { test as TestComponent }