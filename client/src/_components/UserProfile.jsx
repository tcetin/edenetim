import React from 'react';
import { MainLayout, Card, CardBody, CardHeader, CardFooter } from '../_components';
import {config} from '../_helpers';
import { navActions } from "../_actions";
import { connect } from "react-redux";
import MaskedInput from 'react-text-mask';
import Input from '@material-ui/core/Input';
import swal from 'sweetalert2';
import { USER_UPDATE_MUTATION, USER_GET_MUTATION,USER_UPLOAD_AVATAR_MUTATION} from '../_queries';
import { graphql, compose, Query } from 'react-apollo';
import { Loading } from '../assets/devextreme_grid_theme_sources/components/loading';
import Grid from '@material-ui/core/Grid';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';
import { Button } from 'material-ui';
import FlipMove from 'react-flip-move';
import ImageUploader from 'react-images-upload';
import { createUploadLink } from 'apollo-upload-client'
import axios from 'axios';
import gql from 'graphql-tag';

const UploadFile = ({ mutate }) => {
    const handleChange = ({
      target: {
        validity,
        files: [file]
      }
    }) =>
      validity.valid &&
      mutate({
        variables: { file },
        update: (proxy, { data: { singleUpload } }) => {
          const data = proxy.readQuery({ query: uploadsQuery })
          data.uploads.push(singleUpload)
          proxy.writeQuery({ query: uploadsQuery, data })
        }
      })
  
    return <input type="file" required onChange={handleChange} />
  }

class UserProfile extends React.Component {
    constructor(props) {
        super(props)
        let user = JSON.parse(localStorage.getItem('user'))
        this.state = {
            username: '',
            firstName: '',
            lastName: '',
            email: '',
            phone: '',
            unvalidEmail: false,
            id: user.id,
            loading: true,
            selectedFile: null,
            dialogOpen: false,
            uploadStarted: false,
            newImage: ''
        }
        this.fileChangedHandler = this.fileChangedHandler.bind(this)
        this.uploadHandler = this.uploadHandler.bind(this)
        this.toggleUpload = this.toggleUpload.bind(this)
        this.handleFormSubmit = this.handleFormSubmit.bind(this)
    }

    componentDidMount() {
        this.props.dispatch(navActions.active({
            navId: "kullanici_profili"
        }));
        this.getUserData();
    }

    getUserData() {
        let that = this;
        const { id } = this.state
        this.props.getUser({
            variables: { "id": id }
        }).then(result => {
            let _data = result.data.user
            that.setState({ ..._data, loading: false })
        })
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    handleChange(event) {
        let name = event.target.name;
        let value = event.target.value
        this.setState({ [name]: value })

        if (name === "phone") {
            let _val = value.replace(/[^0-9]/g, "")
            this.setState({ phone: _val })
        }

        if (name === "email") {
            //validate email
            let emailIsValid = this.validateEmail(value)
            this.setState({ unvalidEmail: !emailIsValid })
        }
    }

    formSubmit(e) {
        e.preventDefault();

        const { id, username, firstName, lastName, email, phone } = this.state;

        this.props.updateUser({
            variables: { "fields": { id, username, firstName, lastName, email, phone } },
        }).then(result => {
            swal({
                type: 'success',
                title: 'İşlemi Başarılı!',
                text: 'Kullanıcı bilgilerinizi başarıyla güncellediniz.',
                confirmButtonText: 'Tamam'
            })
        }).catch(error => {
            swal({
                type: 'error',
                title: 'Hata oluştu!',
                text: 'Lütfen alanları kontrol ediniz.',
                confirmButtonText: 'Tamam'
            })
        })
    }

    fileChangedHandler(event) {
        this.setState({ selectedFile: event.target.files[0] })
    }

    validFileExtension(file) {
        return (/\.(gif|jpg|jpeg|tiff|png)$/i).test(file)
    }

    uploadHandler() {
        
        const { selectedFile, id } = this.state
        if (this.validFileExtension(selectedFile.name)) {
            this.props.uploadAvatar({
                variables: { id, file: selectedFile }
            }).then(result => {
                console.log(result);
            })
        } else {
            swal({
                type: 'error',
                title: 'Hata oluştu!',
                text: 'Dosya resim formatında olmalı.',
                confirmButtonText: 'Tamam'
            })
        }
    }

    toggleUpload() {
        this.setState({ uploadStarted: !this.state.uploadStarted })
    }

    handleFormSubmit(e){
        e.preventDefault();
        this.props.uploadAvatar({
            variables: { id:this.state.id },
            file:this.state.selectedFile
        }).then(result => {
            console.log(result);
        })
    }

    render() {

        const { username, firstName, lastName, email, phone, unvalidEmail, loading, uploadStarted, newImage, pictures,selectedFile } = this.state;
        let defaultImg = 'dist/assets/img/default-avatar.png'

        return (
            <MainLayout>
                <Grid container spacing={16}>
                    <Grid item xs={8}>
                        <Card iconName="person" >
                            <CardHeader title="Kullanıcı Profili" iconName="person" headerIcon headerRose></CardHeader>
                            <CardBody>
                                {loading && <Loading />}
                                {!loading &&
                                    <form className="form-horizontal" onSubmit={this.formSubmit.bind(this)}>
                                        <div className="row">
                                            <label className="col-md-3 col-form-label">Ad</label>
                                            <div className="col-md-9">
                                                <div className="form-group has-default bmd-form-group">
                                                    <input name="firstName" type="text" className="form-control" value={firstName} onChange={this.handleChange.bind(this)} />
                                                    <p className="text-danger">{!firstName ? "* Ad alanı zorunludur!" : ''}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <label className="col-md-3 col-form-label">Soyad</label>
                                            <div className="col-md-9">
                                                <div className="form-group has-default bmd-form-group">
                                                    <input name="lastName" type="text" className="form-control" value={lastName} onChange={this.handleChange.bind(this)} />
                                                    <p className="text-danger">{!lastName ? "* Soyad alanı zorunludur!" : ''}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <label className="col-md-3 col-form-label">Eposta</label>
                                            <div className="col-md-9">
                                                <div className="form-group has-default bmd-form-group">
                                                    <input name="email" type="text" className="form-control" value={email} onChange={this.handleChange.bind(this)} />
                                                    <p className="text-danger">{!email ? "* Eposta alanı zorunludur!" : ''}</p>
                                                    <p className="text-danger">{unvalidEmail ? "* Eposta formatı uygun değil!" : ''}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <label className="col-md-3 col-form-label">Telefon</label>
                                            <div className="col-md-9">
                                                <div className="form-group has-default bmd-form-group">
                                                    <MaskedInput
                                                        name="phone"
                                                        type="text"
                                                        className="form-control"
                                                        mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
                                                        placeholderChar={'\u2000'}
                                                        showMask
                                                        value={phone}
                                                        onChange={this.handleChange.bind(this)}
                                                    />
                                                    <p className="text-danger">{!phone ? "* Telefon alanı zorunludur!" : ''}</p>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="pull-right">
                                            <button type="submit" className="btn btn-fill btn-success" disabled={!phone || !email || !firstName || !lastName || unvalidEmail}>Profili Güncelle</button>
                                        </div>
                                    </form>}
                            </CardBody>
                        </Card>
                    </Grid>
                    <Grid item xs={4}>
                        <div className="card card-profile">
                            <div className="card-avatar">
                                <img className="img" src={defaultImg} />
                            </div>
                            <div className="card-body">
                                <h6 className="card-category text-gray"></h6>
                                <h4 className="card-title">{firstName}{" "}{lastName}</h4>
                                <p className="card-description"></p>
                                {!uploadStarted && <button type="button" className="btn btn-fill btn-success" onClick={this.toggleUpload.bind(this)}>Profil Resmini Değiştir</button>}
                                {uploadStarted &&
                                    <div className="input-group">
                                        <label className="input-group-btn">
                                            <span className="btn btn-primary" onClick={() => this.fileInput.click()}>
                                                Dosya Seç
                                        </span>
                                            <span className="btn btn-success" onClick={this.uploadHandler}>
                                                Yükle
                                        </span>
                                        </label>
                                        <input type="file" style={{ display: 'none' }}  onChange={this.fileChangedHandler} ref={fileInput => this.fileInput = fileInput} />
                                   
                                    <form method="post" encType="multipart/form-data" onSubmit={this.handleFormSubmit}>
                                        <input type="file" onChange={this.fileChangedHandler}/>
                                        <button type="submit">Ekle</button>
                                    </form>
                                    </div>

                                }
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </MainLayout>
        )
    }
}

function mapStateToProps(state) {
    const { navId } = state.nav;
    return {
        navId
    };
}

const connectedUserProfile = connect(mapStateToProps)(UserProfile);

const composedUserProfile = compose(
    graphql(USER_UPDATE_MUTATION, { name: "updateUser" }),
    graphql(USER_GET_MUTATION, { name: "getUser" }),
    graphql(USER_UPLOAD_AVATAR_MUTATION, { name: "uploadAvatar" }),
)(connectedUserProfile);

export { composedUserProfile as UserProfile }