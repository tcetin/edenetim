import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import Button from '@material-ui/core/Button';
import swal from 'sweetalert2';
import { authHeader, config } from '../_helpers';
import { USER_AUTHENTICATE_MUTATION} from '../_queries';
import { graphql, compose, Query } from 'react-apollo';
import ApolloClient from 'apollo-boost';

const client = new ApolloClient({
    uri: config.apiUrl
});

class Login extends React.Component {
    constructor(props){
        super(props)
        
        // reset login status
        this.props.dispatch(userActions.logout()); 

        this.state = {
            username: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        let that = this;
        if (username && password) {
            // dispatch(userActions.login(username, password));
            this.props.authenticateUser({
                variables: { username,password}
            }).then(function (result) {
                if(result.data.authenticateUser.token) 
                {
                    swal({
                        type: 'success',
                        title: 'Giriş Başarılı!',
                        text: 'Anasayfaya yönlendiriliyorsunuz....',
                        showConfirmButton: false,
                        timer: 2000
                    }).then(() => {
                        that.props.dispatch(userActions.login(result.data.authenticateUser));
                    })
                }
            }).catch(function (error) {
                console.log(error);
            })
        }
    }

    render() {
        const { loggingIn } = this.props;
        const { username, password, submitted } = this.state;
        return (
            <div className="wrapper wrapper-full-page">
                <div className="page-header login-page header-filter" filter-color="black" style={{ backgroundImage: "url('../dist/assets/images/sbbackground.jpg')", backgroundSize: 'cover', backgroundPosition: 'top center' }}>
                    <div className="container">
                        <div className="col-md-4 col-sm-6 ml-auto mr-auto">
                            <form className="form" onSubmit={this.handleSubmit}>
                                <div className="card card-login">
                                    <div className="card-header card-header-rose text-center">
                                        <h4 className="card-title">E-Denetim Kullanıcı Girişi</h4>
                                        <div className="social-line">
                                            <a href="#pablo" className="btn btn-just-icon btn-link btn-white"  style={{minWidth:'200px'}}>
                                                <img src="dist/assets/images/OrtakGirisLogo.svg" alt=""/>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="card-body ">
                                        <span className="bmd-form-group">
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text">
                                                        <i className="material-icons">person</i>
                                                    </span>
                                                </div>
                                                <input type="text" name="username" className="form-control" value={username} placeholder="Kullanıcı Adı..." onChange={this.handleChange}/>
                                            </div>
                                        </span>
                                        <span className="bmd-form-group">
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text">
                                                        <i className="material-icons">lock_outline</i>
                                                    </span>
                                                </div>
                                                <input type="password" name="password" className="form-control" value={password} placeholder="Şifre..." onChange={this.handleChange}/>
                                            </div>
                                        </span>
                                    </div>
                                    <div className="card-footer justify-content-center">
                                        <Button color="secondary" type="submit">Giriş Yap</Button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedLogin = connect(mapStateToProps)(Login);

const composedLogin = compose(
    graphql(USER_AUTHENTICATE_MUTATION, { name: "authenticateUser" })
)(connectedLogin);

export {composedLogin as Login}