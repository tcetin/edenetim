import { navConstants } from '../_constants';

export const navActions = {
    active,
    pushNavActive
};

function active(payload) {
    return { type: navConstants.NAV_ACTIVE, payload };
}

function pushNavActive(payload) {
    return { type: navConstants.PUSH_NAV_ACTIVE, payload };
}