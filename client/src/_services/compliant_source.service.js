import { authHeader, config } from '../_helpers';
import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';

export const compliantSourceService = {
    getAll
};

const client = new ApolloClient({
    uri:"http://localhost:5000/graphql"
});

function getAll() { 
   return(
   client
        .query({
            query: gql`
              {
                allComplaintSourceTypes {
                  name
                  desc
                  createdDate
                  updatedDate
                }
              } `
        })
        .then(result=>result)
    );
}
