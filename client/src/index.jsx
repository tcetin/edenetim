import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { store,config } from './_helpers';
import { App } from './App';
import ApolloClient from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import { createUploadLink } from 'apollo-upload-client'
import {InMemoryCache} from 'apollo-cache-inmemory';

const link = createUploadLink({
    uri:config.apiUrl
})

const client = new ApolloClient({
    link,
    cache: new InMemoryCache(),
});

render(
    <Provider store={store}>
    <ApolloProvider client={client}>
        <App />
    </ApolloProvider>
    </Provider>,
    document.getElementById('app')
);