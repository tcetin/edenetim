import gql from 'graphql-tag';

const SUBJECT_GET_ALL_QUERY = gql`
{
    allSubjects {
      id
      parentId  
      code
      name
      desc
      relativeCreatedDate
      relativeUpdatedDate
      parent {
        id
        parentId
        code
        name
        desc
        relativeCreatedDate
        relativeUpdatedDate
      }
      children {
        id
        parentId
        code
        name
        desc
        relativeCreatedDate
        relativeUpdatedDate
      }
    }
  }
`;

const SUBJECT_CREATE_MUTATION = gql`
  mutation($fields: SubjectInputType!) {
    createSubject(subject: $fields) {
      parentId
      code
      name
      desc
    }
  }
`;

const SUBJECT_UPDATE_MUTATION = gql`
  mutation($fields: SubjectInputType!) {
    updateSubject(subject: $fields) {
      name
      desc
    }
  }
`;

const SUBJECT_DELETE_MUTATION = gql`
  mutation deleteSubject($id: Int!) {
    deleteSubject(Id: $id) {
      name
      desc
    }
  }
`; 

export {SUBJECT_GET_ALL_QUERY,SUBJECT_CREATE_MUTATION,SUBJECT_UPDATE_MUTATION,SUBJECT_DELETE_MUTATION}

