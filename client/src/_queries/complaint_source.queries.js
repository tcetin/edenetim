import gql from 'graphql-tag';

const COMPLAINT_SOURCE_GET_ALL_QUERY = gql`
  {
    allComplaintSourceTypes {
      id
      name
      desc
      relativeCreatedDate
      relativeUpdatedDate
    }
  }
`;

const COMPLAINT_SOURCE_CREATE_MUTATION = gql`
  mutation($fields: CompliantSourceInputType!) {
    createCompliantSource(compliantSource: $fields) {
      name
      desc
    }
  }
`;

const COMPLAINT_SOURCE_UPDATE_MUTATION = gql`
  mutation($fields: CompliantSourceInputType!) {
    updateCompliantSource(compliantSource: $fields) {
      name
      desc
      relativeUpdatedDate
    }
  }
`;

const COMPLAINT_SOURCE_DELETE_MUTATION = gql`
  mutation deleteCompliantSource($id: Int!) {
    deleteCompliantSource(Id: $id) {
      name
      desc
    }
  }
`; 

export {COMPLAINT_SOURCE_GET_ALL_QUERY,COMPLAINT_SOURCE_CREATE_MUTATION,COMPLAINT_SOURCE_UPDATE_MUTATION,COMPLAINT_SOURCE_DELETE_MUTATION}

