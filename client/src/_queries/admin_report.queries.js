import gql from 'graphql-tag';

const ADMIN_REPORT_GET_QUERY = gql`
  {
    adminReport {
      complaintCount
      complaintSourceCount
      subjectChildCount
      subjectParentCount
      taskTypeCount
    }
  }
`;


const UPLOAD_QUERY = gql`{
    allUsers{
      id
    }
  }
`

export {ADMIN_REPORT_GET_QUERY,UPLOAD_QUERY};