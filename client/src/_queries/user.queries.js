import gql from 'graphql-tag';

const USER_GET_ALL_QUERY = gql`
{
  allUsers{
    id
    username
    firstName
    lastName
    email
    phone
    relativeCreatedDate
    relativeUpdatedDate
  }
}
`;

const USER_GET_MUTATION = gql`
mutation($id:Int!){
  user(Id:$id){
      username
      firstName
      lastName
      email
      phone
    }
  }
`;

const USER_CREATE_MUTATION = gql`
  mutation($fields: UserInputType!) {
    createUser(user: $fields) {
     username
    }
  }
`;

const USER_UPDATE_MUTATION = gql`
  mutation($fields: UserInputType!) {
    updateUser(user: $fields) {
      username
    }
  }
`;

const USER_DELETE_MUTATION = gql`
  mutation deleteUser($id: Int!) {
    deleteUser(Id: $id) {
      username
    }
  }
`; 

const USER_AUTHENTICATE_MUTATION = gql`
mutation authenticateUser($username:String!,$password:String!){
  authenticateUser(Username:$username,Password:$password){
    id
    username
    firstName
    lastName
    email
    phone
    token
  }
}
  `;

const USER_UPLOAD_AVATAR_MUTATION = gql`
  mutation uploadUserAvatar($id: Int!) {
    uploadUserAvatar(Id: $id) {
      username
    }
  }
`;

export {USER_GET_ALL_QUERY,USER_CREATE_MUTATION,USER_UPDATE_MUTATION,USER_DELETE_MUTATION,USER_AUTHENTICATE_MUTATION,USER_GET_MUTATION,USER_UPLOAD_AVATAR_MUTATION }
