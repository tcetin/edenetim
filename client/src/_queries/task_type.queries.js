import gql from 'graphql-tag';

const TASKTYPE_GET_ALL_QUERY = gql`
  {
    allTaskTypes {
      id
      name
      shortName
      desc
      relativeCreatedDate
      relativeUpdatedDate
    }
  }
`;

const TASKTYPE_CREATE_MUTATION = gql`
  mutation($fields: TaskTypeInputType!) {
    createTaskType(taskType: $fields) {
      name
      shortName
      desc
    }
  }
`;

const TASKTYPE_UPDATE_MUTATION = gql`
  mutation($fields: TaskTypeInputType!) {
    updateTaskType(taskType: $fields) {
      name
      shortName
      desc
    }
  }
`;

const TASKTYPE_DELETE_MUTATION = gql`
  mutation deleteTaskType($id: Int!) {
    deleteTaskType(Id: $id) {
      name
      shortName
      desc
    }
  }
`; 

export {TASKTYPE_GET_ALL_QUERY,TASKTYPE_CREATE_MUTATION,TASKTYPE_UPDATE_MUTATION,TASKTYPE_DELETE_MUTATION}

