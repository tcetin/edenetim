import gql from 'graphql-tag';

const COMPLAINANT_GET_ALL_QUERY = gql`
  {
    allComplainants {
      id
      name
      desc
      relativeCreatedDate
      relativeUpdatedDate
    }
  }
`;

const COMPLAINANT_CREATE_MUTATION = gql`
  mutation($fields: ComplianantInputType!) {
    createComplainant(complainant: $fields) {
      name
      desc
    }
  }
`;

const COMPLAINANT_UPDATE_MUTATION = gql`
  mutation($fields: ComplianantInputType!) {
    updateComplainant(complainant: $fields) {
      name
      desc
      relativeUpdatedDate
    }
  }
`;

const COMPLAINANT_DELETE_MUTATION = gql`
  mutation deleteComplainant($id: Int!) {
    deleteComplainant(Id: $id) {
      name
      desc
    }
  }
`; 

export {COMPLAINANT_GET_ALL_QUERY,COMPLAINANT_CREATE_MUTATION,COMPLAINANT_UPDATE_MUTATION,COMPLAINANT_DELETE_MUTATION}

