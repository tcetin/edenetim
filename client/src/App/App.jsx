import React from "react";
import { Router, Route, Link, Redirect, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { history } from "../_helpers";
import { alertActions } from "../_actions";
import { PrivateRoute, ComplaintSource, Complainant, TaskType, Subject, Login,User, UserProfile,AdminReport } from "../_components";
import Home from '../HomePage/HomePage';
import classNames from 'classnames';

class App extends React.Component {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    history.listen((location, action) => {
      // clear alert on location change
      dispatch(alertActions.clear());
    });
  }

  render() {
    const { alert } = this.props;
    const privateRoutes = [
      {
        path: "/sikayet_kaynaklari",
        main: ComplaintSource
      },
      {
        path: "/sikayetci",
        main: Complainant
      },
      {
        path: "/gorev_tur",
        main: TaskType
      },
      {
        path: "/konu_kodlari",
        main: Subject
      },
      {
        path: "/kullanicilar",
        main: User
      },
      {
        path: "/kullanici_profili",
        main: UserProfile
      }
    ];

    const publicRoutes = [{
      path: "/giris",
      main: Login
    }]

    return (
      <Router history={history}>
        <div>

        <PrivateRoute exact path='/' component={AdminReport} />

        {privateRoutes.map((route, index) => (
          <PrivateRoute key={index} path={route.path} component={route.main} />
        ))}
        
        {publicRoutes.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            component={route.main}
          />
        ))}
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  const { navId } = state.nav;
  return {
    alert,
    navId
  };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App };
