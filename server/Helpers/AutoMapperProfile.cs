using System.Collections.Generic;
using AutoMapper;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Data.Entities;

namespace WebApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        : this("AutoMapperProfile")
        {
        }
        protected AutoMapperProfile(string profileName)
        : base(profileName)
        {
            CreateMap<ComplaintSource, ComplaintSourceDTO>().ReverseMap();

            CreateMap<Complainant, ComplainantDTO>().ReverseMap();

            CreateMap<TaskType, TaskTypeDTO>().ReverseMap();


            CreateMap<SubjectDTO, Subject>()
            // .ForMember(d => d.Parent.Id, opt => opt.MapFrom(src => src.ParentId))
            .ReverseMap();

             CreateMap<IList<SubjectDTO>, IList<Subject>>();

             CreateMap<UserDTO,User>().ReverseMap();
        }
    }
}