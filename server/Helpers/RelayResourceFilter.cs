using System;
using System.IO;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApi.Helpers
{
    public class RelayResourceFilter : IResourceFilter
    {
        private readonly string jsonMediaType = "application/json";

        public void OnResourceExecuted(ResourceExecutedContext context)
        {
        }

        public void OnResourceExecuting(ResourceExecutingContext context)
        {


            if (!string.Equals(MediaTypeHeaderValue.Parse(context.HttpContext.Request.ContentType).MediaType,
                this.jsonMediaType, StringComparison.OrdinalIgnoreCase))
            {
                var encoder = JavaScriptEncoder.Create();
                var variables = encoder.Encode(context.HttpContext.Request.Form["variables"]);
                var query = encoder.Encode(context.HttpContext.Request.Form["query"]);
                var body = $"{{\"query\":\"{query}\", \"variables\":\"{variables}\"}}";

                byte[] requestData = Encoding.UTF8.GetBytes(body);
                context.HttpContext.Request.Body = new MemoryStream(requestData);
                context.HttpContext.Request.ContentType = this.jsonMediaType;
            }
        }
    }
}