using System;

namespace WebApi.Helpers
{
    public static class DateTimeExtension
    {
        public static string TimeAgo(this DateTime? dateTime)
        {
            string result = string.Empty;

            if (dateTime == null)
            {
                return result;
            }

            var dt = dateTime.Value;

            var timeSpan = DateTime.Now.Subtract(dt);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                result = string.Format("{0} Saniye Önce", timeSpan.Seconds);
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format("Yaklaşık {0} Dakika Önce", timeSpan.Minutes) :
                    "Yaklaşık 1 Dakika Önce";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format("Yaklaşık {0} Saat Önce", timeSpan.Hours) :
                    "Yaklaşık 1 Saat Önce";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                result = timeSpan.Days > 1 ?
                    String.Format("Yaklaşık {0} Gün Önce", timeSpan.Days) :
                    "Dün";
            }
            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30 ?
                    String.Format("Yaklaşık {0} Ay Önce", timeSpan.Days / 30) :
                    "Yaklaşık 1 Ay Önce";
            }
            else
            {
                result = timeSpan.Days > 365 ?
                    String.Format("Yaklaşık {0} Yıl Önce", timeSpan.Days / 365) :
                    "Yaklaşık 1 Yıl Önce";
            }

            return result;
        }
    }
}
