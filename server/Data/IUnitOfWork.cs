using System;
 
namespace WebApi.Data
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<T> GetRepository<T>() where T : class;
        int SaveChanges();
    }
}