using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApi.Data.Entities.Models;

namespace WebApi.Data.Mapping
{
    public class ComplainantConfiguration : IEntityTypeConfiguration<Complainant>
    {
        public void Configure(EntityTypeBuilder<Complainant> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name).HasMaxLength(200).IsRequired();
            builder.Property(c => c.Desc).HasMaxLength(500);
            builder.Property(c => c.IsDeleted).HasDefaultValue(false);

            builder.ToTable("Complainants");
        }
    }
}