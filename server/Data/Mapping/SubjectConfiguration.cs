using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApi.Data.Entities.Models;

namespace WebApi.Data.Mapping
{
    public class SubjectConfiguration : IEntityTypeConfiguration<Subject>
    {
        public void Configure(EntityTypeBuilder<Subject> builder)
        {
            #region primary_key
            builder.HasKey(c => c.Id);
            #endregion

            #region properties
            builder.Property(c => c.Code).HasMaxLength(200).IsRequired();
            builder.Property(c => c.Name).HasMaxLength(200).IsRequired();
            builder.Property(c => c.Desc).HasMaxLength(500);
            #endregion

            #region relationships
            builder.HasMany(c => c.Children).WithOne(c => c.Parent).OnDelete(DeleteBehavior.SetNull); ;
            #endregion

            #region index
            builder.HasIndex(p => p.Code).IsUnique();
            #endregion

            #region table
            builder.ToTable("Subjects");
            #endregion
        }
    }
}