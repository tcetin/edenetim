using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApi.Data.Entities.Models;

namespace WebApi.Data.Mapping
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Username).HasMaxLength(200).IsRequired();
            builder.Property(c => c.FirstName).HasMaxLength(200).IsRequired();
            builder.Property(c => c.LastName).HasMaxLength(200).IsRequired();
            builder.Property(c => c.Email).HasMaxLength(200).IsRequired();
            builder.Property(c => c.Phone).HasMaxLength(200).IsRequired();
            builder.ToTable("Users");
        }
    }
}