using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApi.Data.Entities.Models;

namespace WebApi.Data.Mapping
{
    public class TaskTypeConfiguration : IEntityTypeConfiguration<TaskType>
    {
        public void Configure(EntityTypeBuilder<TaskType> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name).HasMaxLength(200).IsRequired();
            builder.Property(c => c.ShortName).HasMaxLength(10).IsRequired();
            builder.Property(c => c.Desc).HasMaxLength(1000);
            builder.Property(c => c.IsDeleted).HasDefaultValue(false);

            builder.ToTable("TaskTypes");
        }
    }
}