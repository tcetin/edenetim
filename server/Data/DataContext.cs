//Copyright 2017 (c) SmartIT. All rights reserved. By John Kocer  
using System;  
using System.Linq;  
using System.Security.Principal;  
using System.Threading;  
using System.Threading.Tasks;  
using Microsoft.EntityFrameworkCore;  
using WebApi.Data.Entities.Models;
using WebApi.Data.Mapping;

namespace WebApi.Data 
{  
  public class DataContext : DbContext  
  {  
    public DbSet<ComplaintSource> ComplaintSources { get; set; } //Şikayet Kaynakları bimer cimer vs.
    public DbSet<Complainant> Complainants { get; set; } //Şikayetçi türü Şahıs kurum vs.
    public DbSet<TaskType> TaskTypes { get; set; } // Görev Türleri(Müfettişler için) Toplantı,Görüş Verme vs.
    public DbSet<Subject> Subjects { get; set; } //Konu kodları
    public DbSet<User> User { get; set; }

    public DataContext(DbContextOptions<DataContext> options) : base(options){}  
  
    protected override void OnModelCreating(ModelBuilder builder)  
    {  
    
      base.OnModelCreating(builder);  

      builder.ApplyConfiguration(new ComplaintSourceConfiguration());
      builder.ApplyConfiguration(new ComplainantConfiguration());
      builder.ApplyConfiguration(new TaskTypeConfiguration());
      builder.ApplyConfiguration(new SubjectConfiguration());
      builder.ApplyConfiguration(new UserConfiguration());
    }  

  }  
}  