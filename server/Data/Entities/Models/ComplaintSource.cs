using System;

namespace WebApi.Data.Entities.Models
{
    public class ComplaintSource : Auditable
    {
        public string Name { get; set; }
        public string Desc { get; set; }
        public bool? IsDeleted { get; set; }
    }
}