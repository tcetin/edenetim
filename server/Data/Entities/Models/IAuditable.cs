using System;
using WebApi.Helpers;

namespace WebApi.Data.Entities.Models
{
    public interface IAuditable
    {
        string CreatedBy { get; set; }
        DateTime? CreatedDate { get; set; }
        string UpdatedBy { get; set; }
        DateTime? UpdatedDate { get; set; }
    }

    public class Auditable : IAuditable
    {
        public int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }


    public class AuditableDTO : IAuditable
    {
        public int Id { get; set; }
        private DateTime? dateCreated = null;
        private DateTime? dateUpdated = null;

        public string CreatedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? CreatedDate
        {
            get
            {
                return this.dateCreated.HasValue ? this.dateCreated.Value : DateTime.Now;
            }
            set
            {
                this.dateCreated = value;
            }
        }
        public string RelativeCreatedDate
        {
            get
            {
                return DateTimeExtension.TimeAgo(this.CreatedDate);
            }
        }
        public string UpdatedBy { get; set; }
        public string RelativeUpdatedDate
        {
            get
            {
                return DateTimeExtension.TimeAgo(this.UpdatedDate);
            }
        }
        public DateTime? UpdatedDate
        {
            get
            {
                return this.dateUpdated.HasValue ? this.dateUpdated.Value : DateTime.Now;
            }
            set
            {
                this.dateUpdated = value;
            }
        }
    }

}