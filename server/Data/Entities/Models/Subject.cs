using System;
using System.Collections.Generic;

namespace WebApi.Data.Entities.Models
{
    public class Subject : Auditable
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public virtual Subject Parent { get; set; }
        public virtual ICollection<Subject> Children { get; set; }
    }
}