using System;
using GraphQL.Types;
using WebApi.Data.Entities.Graphql.GraphQueryTypes;

namespace WebApi.Data.Entities.Graphql
{
    public class GraphQLSchema: Schema
    {
        public GraphQLSchema(Func<Type, GraphType> resolveType)
            :base(resolveType)
        {
            Query = (RootQuery)resolveType(typeof(RootQuery));
            Mutation = (RootMutation)resolveType(typeof(RootMutation));
        }
    }
}