using GraphQL.Types;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Helpers;
using WebApi.Services;

namespace WebApi.Data.Entities.Graphql.GraphQueryTypes
{
    public class RootQuery : ObjectGraphType
    {
        public RootQuery(
        IComplaintSourceService complaintSourceService,
        IComplainantService complainantService,
        ITaskTypeService taskTypeService,
        ISubjectService subjectService,
        IUserService userService,
        IReportService reportService
        )
        {

            #region ComplaintSourceType
            Field<ComplaintSourceType>(
                "complaintSourceType",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "Id", Description = "Identity Alanı" }
                ),
                resolve: context =>
                {
                    var model = complaintSourceService.GetById(context.GetArgument<int>("Id"));
                    return model;
                }
            );

            Field<ListGraphType<ComplaintSourceType>>(
                "allComplaintSourceTypes",
                resolve: context =>
                {
                    var result = complaintSourceService.GetAllComplaints();
                    return result;
                }
            );

            #endregion

            #region ComplainantGraphType
           
           Field<ComplainantGraphType>(
                "complainantGraphType",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "Id", Description = "Identity Alanı" }
                ),
                resolve: context =>
                {
                    var model = complainantService.GetById(context.GetArgument<int>("Id"));
                    return model;
                }
            );

            Field<ListGraphType<ComplainantGraphType>>(
                "allComplainants",
                resolve: context =>
                {
                    var result = complainantService.GetAllComplainants();
                    return result;
                }
            );
            #endregion

            #region TaskTypeGraphType
            Field<TaskTypeGraphType>(
                "taskTypeGraphType",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "Id", Description = "Identity Alanı" }
                ),
                resolve: context =>
                {
                    var model = taskTypeService.GetById(context.GetArgument<int>("Id"));
                    return model;
                }
            );

            Field<ListGraphType<TaskTypeGraphType>>(
                "allTaskTypes",
                resolve: context =>
                {
                    var result = taskTypeService.GetAllTaskTypes();
                    return result;
                }
            );
            #endregion

            #region SubjectGraphType
             Field<SubjectGraphType>(
                "subject",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "Id", Description = "Identity Alanı" }
                ),
                resolve: context =>
                {
                    var model = subjectService.GetById(context.GetArgument<int>("Id"));
                    return model;
                }
            );

            Field<ListGraphType<SubjectGraphType>>(
                "allSubjects",
                resolve: context =>
                {
                    var model = subjectService.GetAllSubjects();
                    return model;
                }
            );
            #endregion

            #region UserGraphType

            Field<ListGraphType<UserGraphType>>(
                "allUsers",
                resolve: context =>
                {
                    var model = userService.GetAllUsers();
                    return model;
                }
            );

            Field<AdminReportGraphType>(
                "adminReport",
                resolve: context =>
                {
                    var model = reportService.GetAdminReport();
                    return model;
                }
            );

            #endregion
        }
    }
}