using GraphQL.Types;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Services;

namespace WebApi.Data.Entities.Graphql.GraphQueryTypes
{
    public class ComplaintSourceType : ObjectGraphType<ComplaintSourceDTO>
    {
        public ComplaintSourceType(IComplaintSourceService service)
        {
            Field(x => x.Id).Description("Id.");
            Field(x => x.Name).Description("Şikayet Kaynağı Adı");
            Field(x => x.Desc).Description("Açıklama");
            Field(x => x.CreatedDate,nullable:true).Description("Oluşturulma Tarihi");
            Field(x => x.CreatedBy).Description("Oluşturan Kullanıcı");
            Field(x => x.UpdatedDate,nullable:true).Description("Güncellenme Tarihi");
            Field(x => x.UpdatedBy).Description("Güncelleyen Kullanıcı");
            Field(x => x.RelativeCreatedDate,nullable:true).Description("Oluşturulma Tarihi(Relative)");
            Field(x => x.RelativeUpdatedDate,nullable:true).Description("Güncellenme Tarihi(Relative)");
        }
    }
}
