using System.IO;
using GraphQL;
using GraphQL.Types;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using WebApi.Data.Entities.Graphql.InputTypes;
using WebApi.Dtos;
using WebApi.Services;

namespace WebApi.Data.Entities.Graphql.GraphQueryTypes
{
    public class RootMutation : ObjectGraphType<object>
    {
        public RootMutation(
            IComplaintSourceService complaintSourceService, 
            IComplainantService complainantService,
            ITaskTypeService taskTypeService,
            ISubjectService subjectService,
            IUserService userService
            )
        {
            Name = "RootMutation";

            #region CompliantSource
            #region CreateCompliantSource

            Field<ComplaintSourceType>(
                "createCompliantSource",
                Description = "Yeni bir şikayet kaynağı ekler.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<CompliantSourceInputType>> { Name = "compliantSource" }
            ),
            resolve: context =>
            {
                var compliantSource = context.GetArgument<ComplaintSourceDTO>("compliantSource");
                return complaintSourceService.Create(compliantSource);
            });

            #endregion

            #region UpdateCompliantSource

            Field<ComplaintSourceType>(
                "updateCompliantSource",
                Description = "Mevcut şikayet kaynağını günceller.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<CompliantSourceInputType>> { Name = "compliantSource" }
            ),
            resolve: context =>
            {
                var compliantSource = context.GetArgument<ComplaintSourceDTO>("compliantSource");
                return complaintSourceService.Update(compliantSource);
            });

            #endregion

            #region SoftDeleteCompliantSource

            Field<ComplaintSourceType>(
                "deleteCompliantSource",
                Description = "Mevcut şikayet kaynağını siler.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "Id", Description = "Identity Alanı" }
            ),
            resolve: context =>
            {
                return complaintSourceService.SoftDelete(context.GetArgument<int>("Id"));
            });

            #endregion
            #endregion

            #region Complainant
            #region CreateComplainant

            Field<ComplainantGraphType>(
                "createComplainant",
                Description = "Yeni bir şikayetçi  ekler.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<ComplainantInputType>> { Name = "complainant" }
            ),
            resolve: context =>
            {
                var compliant = context.GetArgument<ComplainantDTO>("complainant");
                return complainantService.Create(compliant);
            });

            #endregion

            #region UpdateComplainant

            Field<ComplainantGraphType>(
                "updateComplainant",
                Description = "Mevcut şikayetçiyi günceller.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<ComplainantInputType>> { Name = "complainant" }
            ),
            resolve: context =>
            {
                var compliant = context.GetArgument<ComplainantDTO>("complainant");
                return complainantService.Update(compliant);
            });

            #endregion

            #region SoftDeleteComplainant
            Field<ComplainantGraphType>(
                "deleteComplainant",
                Description = "Mevcut şikayetçiyi siler.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "Id", Description = "Identity Alanı" }
            ),
            resolve: context =>
            {
                return complainantService.SoftDelete(context.GetArgument<int>("Id"));
            });

            #endregion
            #endregion

            #region TaskType
            #region CreateTaskType

            Field<TaskTypeGraphType>(
                "createTaskType",
                Description = "Yeni bir görev türü  ekler.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<TaskTypeInputType>> { Name = "taskType" }
            ),
            resolve: context =>
            {
                var taskType = context.GetArgument<TaskTypeDTO>("taskType");
                return taskTypeService.Create(taskType);
            });

            #endregion

            #region UpdateTaskType

            Field<TaskTypeGraphType>(
                "updateTaskType",
                Description = "Mevcut görev türünü günceller.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<TaskTypeInputType>> { Name = "taskType" }
            ),
            resolve: context =>
            {
                var taskType = context.GetArgument<TaskTypeDTO>("taskType");
                return taskTypeService.Update(taskType);
            });

            #endregion

            #region SoftDeleteTaskType
            Field<TaskTypeGraphType>(
                "deleteTaskType",
                Description = "Mevcut görev türünü siler.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "Id", Description = "Identity Alanı" }
            ),
            resolve: context =>
            {
                return taskTypeService.SoftDelete(context.GetArgument<int>("Id"));
            });

            #endregion
            #endregion

            #region Subject

            Field<SubjectGraphType>(
                "createSubject",
                Description = "Yeni bir konu kodu ekler.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<SubjectInputType>> { Name = "subject" }
            ),
            resolve: context =>
            {
                var subject = context.GetArgument<SubjectDTO>("subject");
                return subjectService.Create(subject);
            });

            Field<SubjectGraphType>(
                "updateSubject",
                Description = "Mevcut konu kodunu günceller.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<SubjectInputType>> { Name = "subject" }
            ),
            resolve: context =>
            {
                var subject = context.GetArgument<SubjectDTO>("subject");
                return subjectService.Update(subject);
            });

           Field<SubjectGraphType>(
                "deleteSubject",
                Description = "Mevcut konu kodunu siler.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "Id", Description = "Identity Alanı" }
            ),
            resolve: context =>
            {
                return subjectService.SoftDelete(context.GetArgument<int>("Id"));
            });

            #endregion

            #region User
            Field<UserGraphType>(
                "createUser",
                Description = "Yeni bir kullanıcı ekler.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<UserInputType>> { Name = "user" }
            ),
            resolve: context =>
            {
                var user = context.GetArgument<UserDTO>("user");
                return userService.Create(user);
            });

            Field<UserGraphType>(
                "updateUser",
                Description = "Mevcut kullanıcıyı günceller.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<UserInputType>> { Name = "user" }
            ),
            resolve: context =>
            {
                var user = context.GetArgument<UserDTO>("user");
                return userService.Update(user);
            });

            Field<UserGraphType>(
                "deleteUser",
                Description = "Mevcut kullanıcıyı siler.",
                arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "Id", Description = "Identity Alanı" }
            ),
            resolve: context =>
            {
                return userService.Delete(context.GetArgument<int>("Id"));
            });

            Field<UserGraphType>(
                "authenticateUser",
                Description="Yetkilendirme işlemi",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "Username", Description = "Kullanıcı Adı" },
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "Password", Description = "Şifre" }
                ),
                resolve: context =>
                {
                    var model = userService.Authenticate(context.GetArgument<string>("Username"),context.GetArgument<string>("Password"));
                    return model;
                }
            );

             Field<UserGraphType>(
                "user",
                Description="Kullanıcı bilgisi.",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "Id", Description = "Identity Alanı" }
                ),
                resolve: context =>
                {
                    var model = userService.GetById(context.GetArgument<int>("Id"));
                    return model;
                }
            ); 

            Field<UserGraphType>(
                "uploadUserAvatar",
                Description="Kullanıcı resmi yükleme.",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "Id", Description = "Identity Alanı" }
                ),
                resolve: context => 
                {
                    var file = context.UserContext.As<IFormFile>();
                    var model = userService.UploadAvatar(context.GetArgument<int>("Id"),file);
                    return null;
                }
            ); 

            #endregion
        }
    }
}