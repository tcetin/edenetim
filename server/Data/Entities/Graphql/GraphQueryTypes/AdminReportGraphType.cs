using GraphQL.Types;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Services;

namespace WebApi.Data.Entities.Graphql.GraphQueryTypes
{
    public class AdminReportGraphType : ObjectGraphType<AdminReportDTO>
    {
        public AdminReportGraphType()
        {
            Field(x => x.ComplaintCount).Description("Şikayetçi Sayısı");
            Field(x => x.ComplaintSourceCount).Description("Şikayet Kaynağı Sayısı");
            Field(x => x.SubjectParentCount).Description("Ana Konu Kodu Sayısı");
            Field(x => x.SubjectChildCount).Description("Alt Konu Kodu Sayısı");
            Field(x => x.TaskTypeCount).Description("Görev Türü Sayısı");
        }
    }
}