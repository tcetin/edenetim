using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using GraphQL.Types;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Services;

namespace WebApi.Data.Entities.Graphql.GraphQueryTypes
{
    public class SubjectGraphType : ObjectGraphType<SubjectDTO>
    {
        private IMapper _mapper;
        public SubjectGraphType(ISubjectService service,IMapper mapper)
        {
            _mapper = mapper;
            
            Field(x => x.Id).Description("Id.");
            Field(x => x.ParentId).Description("Parent Id.");
            Field(x => x.Code).Description("Konu Kodu");
            Field(x => x.Name).Description("Konu Adı");
            Field(x => x.Desc).Description("Açıklama");
            Field<SubjectGraphType>(
                "parent",
                "Üst Konu",
                resolve: context =>
                {
                    var _parent = _mapper.Map<SubjectDTO>(context.Source.Parent);
                    return _parent;
                }
            ); 
            Field<ListGraphType<SubjectGraphType>>(
                "children",
                "Alt Konular",
                resolve: context =>
                {
                    var _children = _mapper.Map<List<SubjectDTO>>(context.Source.Children);
                    return _children;
                }
            );
            Field(x => x.CreatedDate,nullable:true).Description("Oluşturulma Tarihi");
            Field(x => x.CreatedBy).Description("Oluşturan Kullanıcı");
            Field(x => x.UpdatedDate,nullable:true).Description("Güncellenme Tarihi");
            Field(x => x.UpdatedBy).Description("Güncelleyen Kullanıcı");
            Field(x => x.RelativeCreatedDate,nullable:true).Description("Oluşturulma Tarihi(Relative)");
            Field(x => x.RelativeUpdatedDate,nullable:true).Description("Güncellenme Tarihi(Relative)");
        }

    }

    // public class SubjectGraphMutationType : ObjectGraphType<PlainSubjectDTO>
    // {
    //     public SubjectGraphMutationType(ISubjectService service)
    //     {
    //         Field(x => x.Id).Description("Id.");
    //         Field(x => x.Code).Description("Konu Kodu");
    //         Field(x => x.Name).Description("Konu Adı");
    //         Field(x => x.Desc).Description("Açıklama");
    //     }

    // }
    
}