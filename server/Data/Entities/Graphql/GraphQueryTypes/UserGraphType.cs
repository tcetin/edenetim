using GraphQL.Types;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Services;

namespace WebApi.Data.Entities.Graphql.GraphQueryTypes
{
    public class UserGraphType : ObjectGraphType<UserDTO>
    {
        public UserGraphType(IUserService service)
        {
            Field(x => x.Id).Description("Id.");
            Field(x => x.Username).Description("Kullanıcı Adı");
            Field(x => x.FirstName).Description("Ad");
            Field(x => x.LastName).Description("SoyAd");
            Field(x => x.Email).Description("Ad");
            Field(x => x.Phone).Description("SoyAd");
            Field(x => x.Password).Description("Şifre");
            Field(x => x.CreatedDate,nullable:true).Description("Oluşturulma Tarihi");
            Field(x => x.CreatedBy).Description("Oluşturan Kullanıcı");
            Field(x => x.UpdatedDate,nullable:true).Description("Güncellenme Tarihi");
            Field(x => x.UpdatedBy).Description("Güncelleyen Kullanıcı");
            Field(x => x.RelativeCreatedDate,nullable:true).Description("Oluşturulma Tarihi(Relative)");
            Field(x => x.RelativeUpdatedDate,nullable:true).Description("Güncellenme Tarihi(Relative)");
             Field(x => x.Token).Description("Token");
        }
    }
}