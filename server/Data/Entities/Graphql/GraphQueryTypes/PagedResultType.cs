using GraphQL.Types;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Helpers;
using WebApi.Services;

namespace WebApi.Data.Entities.Graphql.GraphQueryTypes
{
    public class PagedResultType: ObjectGraphType<PagingDTO>
    {
        public PagedResultType(IComplaintSourceService service)
        {
            Field(x => x.RowCount).Description("Satır Sayısı");
            Field(x => x.PageCount).Description("Sayfa Sayısı");
            Field(x => x.PageSize).Description("Bir sayfadaki satır sayısı");
            // Field(x => x.LastRowOnPage).Description("Son Satır");
            // Field(x => x.FirstRowOnPage).Description("İlk satır");
            Field(x => x.CurrentPage).Description("Mevcut sayfa");
        }
    }
}
