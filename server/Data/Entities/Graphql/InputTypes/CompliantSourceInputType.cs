using GraphQL.Types;

namespace WebApi.Data.Entities.Graphql.InputTypes
{
    public class CompliantSourceInputType : InputObjectGraphType
    {
        public CompliantSourceInputType()
        {
            Name = "CompliantSourceInputType";
            Field<IntGraphType>("Id");
            Field<NonNullGraphType<StringGraphType>>("Name");
            Field<NonNullGraphType<StringGraphType>>("Desc");
        }
    }
}