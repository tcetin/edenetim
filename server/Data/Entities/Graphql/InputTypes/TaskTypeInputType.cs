using GraphQL.Types;

namespace WebApi.Data.Entities.Graphql.InputTypes
{
    public class TaskTypeInputType : InputObjectGraphType
    {
        public TaskTypeInputType()
        {
            Name = "TaskTypeInputType";
            Field<IntGraphType>("Id");
            Field<NonNullGraphType<StringGraphType>>("Name");
            Field<NonNullGraphType<StringGraphType>>("ShortName");
            Field<NonNullGraphType<StringGraphType>>("Desc");
        }
    }
}