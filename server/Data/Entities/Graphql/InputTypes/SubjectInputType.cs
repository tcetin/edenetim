using GraphQL.Types;

namespace WebApi.Data.Entities.Graphql.InputTypes
{
    public class SubjectInputType : InputObjectGraphType
    {
        public SubjectInputType()
        {
            Name = "SubjectInputType";
            Field<IntGraphType>("Id");
            Field<IntGraphType>("ParentId");
            Field<NonNullGraphType<StringGraphType>>("Code");
            Field<NonNullGraphType<StringGraphType>>("Name");
            Field<NonNullGraphType<StringGraphType>>("Desc");
        }
    }
}