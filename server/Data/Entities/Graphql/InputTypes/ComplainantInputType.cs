using GraphQL.Types;

namespace WebApi.Data.Entities.Graphql.InputTypes
{
    public class ComplainantInputType : InputObjectGraphType
    {
        public ComplainantInputType()
        {
            Name = "ComplianantInputType";
            Field<IntGraphType>("Id");
            Field<NonNullGraphType<StringGraphType>>("Name");
            Field<NonNullGraphType<StringGraphType>>("Desc");
        }
    }
}