// using System;
// using System.Collections.Generic;
// using System.Data;
// using System.Linq;
// using System.Threading.Tasks;
// using Dapper;
// using Microsoft.Extensions.Configuration;
// using Oracle.ManagedDataAccess.Client;
// using WebApi.Entities;
// using WebApi.Helpers;
// using WebApi.Oracle;

// namespace WebApi.Services
// {
//     public interface IHospitalService
//     {
//         object GetHospitalsCountByCity();
//         object GetHospitalsByType(int type);
//         object AllHospitals(int? type);
//         Task<List<Hospital>> GetHospitalsAsync(string typeId);
//         Task<Hospital> GetHospitalAsync(int hospitalCode); 
//     }

//     public class HospitalService : IHospitalService
//     {
//         private IConfiguration _configuration;
//         private List<Hospital> _hospitals;

//         public HospitalService(IConfiguration configuration)
//         {
//             _configuration = configuration;

//             using (var conn = this.GetConnection())
//             {
//                 if (conn.State == ConnectionState.Closed)
//                 {
//                     conn.Open();
//                 }

//                 if (conn.State == ConnectionState.Open)
//                 {
//                     var query = "SELECT ROW_ID,KOD_IL,KOD_MAP,ILAD,KURUM_KODU,KURUM_ADI,KOORDINAT,YATAK_ADEDI,KOORDINATLINK,KURUM_TIP FROM V_KHGM_HARITA_SAGLIK_TESISLERI";
//                     _hospitals = conn.Query<Hospital>(query).ToList();
//                 }
//             }
//         }

//         /**Tüm Kurumlar
//         */
//         public object AllHospitals(int? type)
//         {
//              object result = null;

//             try
//             {
//                using (var conn = this.GetConnection())
//                 {
//                     if (conn.State == ConnectionState.Closed)
//                     {
//                         conn.Open();
//                     }

//                     if (conn.State == ConnectionState.Open)
//                     {
//                         var query = string.Empty;

//                         if (type != null && type!=1)
//                         {
//                             query = "SELECT ROW_ID,KOD_IL,KOD_MAP,ILAD,KURUM_KODU,KURUM_ADI,KOORDINAT,YATAK_ADEDI,KOORDINATLINK,KURUM_TIP FROM V_KHGM_HARITA_SAGLIK_TESISLERI WHERE KURUM_TIP=:KURUM_TIP";
//                             result = conn.Query<Hospital>(query, new { KURUM_TIP = type }).ToList();
//                         }
//                         else if (type == 1 || type == null)
//                         {
//                             query = "SELECT ROW_ID,KOD_IL,KOD_MAP,ILAD,KURUM_KODU,KURUM_ADI,KOORDINAT,YATAK_ADEDI,KOORDINATLINK,KURUM_TIP FROM V_KHGM_HARITA_SAGLIK_TESISLERI";
//                             result = conn.Query<Hospital>(query, new { KURUM_TIP = type }).ToList();
//                         }

//                     }
//                 }
//             }
//             catch (System.Exception)
//             {
                
//                 throw;
//             }

//             return result;
//         }

//         /**
//          *İl haritası için tüm hastaneler
//          */
//         public object GetHospitalsCountByCity()
//         {
//             object result = null;

//             try
//             {
//                 using (var conn = this.GetConnection())
//                 {
//                     if (conn.State == ConnectionState.Closed)
//                     {
//                         conn.Open();
//                     }

//                     if (conn.State == ConnectionState.Open)
//                     {
//                         var query = "SELECT KOD_MAP AS ID,COUNT(*) AS VALUE FROM V_KHGM_HARITA_SAGLIK_TESISLERI GROUP BY KOD_MAP";
//                         //result = SqlMapper.Query(conn,query).ToList();
//                         result = conn.Query<HospitalMap>(query).ToList();
//                     }
//                 }


//             }
//             catch (System.Exception)
//             {
                
//                 throw;
//             }

//             return result;
//         }

//         public object GetHospitalsByType(int typeId)
//         {
//             object result = null;

//             try
//             {
//                 using (var conn = this.GetConnection())
//                 {
//                     if (conn.State == ConnectionState.Closed)
//                     {
//                         conn.Open();
//                     }

//                     if (conn.State == ConnectionState.Open)
//                     {

//                         // var dyParam = new OracleDynamicParameters();
//                         // dyParam.Add("KURUM_TIP", OracleDbType.Int32, ParameterDirection.Input, typeId);
//                         // dyParam.Add("DETAIL_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);
//                         // var query = "SPTS.SP_KHB_HOSPITALS";
//                         // result = SqlMapper.Query(conn, query, param: dyParam, commandType: CommandType.StoredProcedure);

//                         if (typeId == 1)
//                         {
//                             //Tüm Hastaneler
//                             var squery = "SELECT KOD_MAP AS ID,COUNT(*) AS VALUE FROM V_KHGM_HARITA_SAGLIK_TESISLERI  GROUP BY KOD_MAP";
//                             result = conn.Query<HospitalMap>(squery).ToList();
//                         }
//                         else
//                         {
//                             var query = "SELECT KOD_MAP AS ID,COUNT(*) AS VALUE FROM V_KHGM_HARITA_SAGLIK_TESISLERI WHERE KURUM_TIP=:KURUM_TIP GROUP BY KOD_MAP";
//                             result = conn.Query<HospitalMap>(query, new { KURUM_TIP = typeId }).ToList();
//                         }
//                     }
//                 }
                
//             }
//             catch (Exception ex)
//             {
                
//                 throw ex;
//             }

//             return result;
//         }

//         public IDbConnection GetConnection()
//         {
//             var connectionString = _configuration.GetSection("ConnectionStrings").GetSection("SptsConnection").Value;
//             var conn = new OracleConnection(connectionString);           
//             return conn;
//         }

//         public Task<Hospital> GetHospitalAsync(int hospitalCode)
//         {
//             return Task.FromResult(_hospitals.FirstOrDefault(x => x.KURUM_KODU == hospitalCode));
//         }

//         public Task<List<Hospital>> GetHospitalsAsync(string typeId)
//         {
//             if (!string.IsNullOrEmpty(typeId))
//             {
//                 var result = _hospitals.Where(x => x.KURUM_TIP == typeId).ToList();
//                 return Task.FromResult(result);
//             }
//             return Task.FromResult(_hospitals);
//         }
//     }
// }