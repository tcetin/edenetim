using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Helpers;

namespace WebApi.Services.Concrete
{
    public class ReportService : IReportService
    {


        private DataContext _dbContext;
        private UnitOfWork _uow;
        private IMapper _mapper;
        private IGenericRepository<Complainant> _complaintRepository;
        private IGenericRepository<ComplaintSource> _complaintSourceRepository;
        private IGenericRepository<Subject> _subjectRepository;
        private IGenericRepository<TaskType> _taskTypeRepository;
        public ReportService(DataContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _uow = new UnitOfWork(_dbContext);
            _complaintRepository = _uow.GetRepository<Complainant>();
            _complaintSourceRepository = _uow.GetRepository<ComplaintSource>();
            _subjectRepository = _uow.GetRepository<Subject>();
            _taskTypeRepository = _uow.GetRepository<TaskType>();
            _mapper = mapper;
        }

        public AdminReportDTO GetAdminReport(){
            try
            {
                var _dto = new AdminReportDTO
                {
                    ComplaintCount = _complaintRepository.FindBy(p => p.IsDeleted == false).Count(),
                    ComplaintSourceCount = _complaintSourceRepository.FindBy(p => p.IsDeleted == false).Count(),
                    SubjectParentCount = _subjectRepository.FindBy(p => p.Parent == null || p.Children.Count() > 0).Count(),
                    SubjectChildCount = _subjectRepository.FindBy(p => p.Parent != null).Count(),
                    TaskTypeCount = _taskTypeRepository.FindBy(p => p.IsDeleted == false).Count()
                };

                return _dto;

            }
            catch (System.Exception)
            {
                
                throw;
            }

        }
    }
}