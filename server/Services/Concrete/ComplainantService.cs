using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Helpers;

namespace WebApi.Services.Concrete
{
    public class ComplainantService : IComplainantService
    {
        private DataContext _dbContext;
        private UnitOfWork _uow;
        private IMapper _mapper;
        private IGenericRepository<Complainant> _repository;
        public ComplainantService(DataContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _uow = new UnitOfWork(_dbContext);
            _repository = _uow.GetRepository<Complainant>();
            _mapper = mapper;
        }

        public IEnumerable<Complainant> All => _repository.GetAll();

        public ComplainantDTO Create(ComplainantDTO dto)
        {
            try
            {
                var _entity = _mapper.Map<Complainant>(dto);
                _repository.Add(_entity);
                _uow.SaveChanges();
                var _dto = _mapper.Map<ComplainantDTO>(_entity);
                return _dto;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public ComplainantDTO SoftDelete(int id)
        {
            try
            {
                var entity = _repository.Get(id);
                entity.IsDeleted = true;
                _repository.Update(entity, id);
                _uow.SaveChanges();
                return _mapper.Map<ComplainantDTO>(entity);
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        public ComplainantDTO GetById(int id)
        {
            try
            {
                var _source = _repository.Get(id);
                var _dto = _mapper.Map<ComplainantDTO>(_source);
                return _dto;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<ComplainantDTO>> GetAllComplainants()
        {
            try
            {
                var _data = await _repository.FindBy(x => x.IsDeleted == false).ToListAsync();

                return _mapper.Map<List<ComplainantDTO>>(_data).OrderByDescending(p => p.Id);
            }
            catch (System.Exception)
            {

                throw;
            }
        }



        public ComplainantDTO Update(ComplainantDTO dto)
        {
            try
            {
                var entity = _repository.Get(dto.Id);
                entity.Name = dto.Name;
                entity.Desc = dto.Desc;
                entity.UpdatedDate = DateTime.Now;
                _repository.Update(entity, dto.Id);
                _uow.SaveChanges();
                return _mapper.Map<ComplainantDTO>(entity);
            }
            catch (System.Exception)
            {

                throw;
            }
        }
    }
}