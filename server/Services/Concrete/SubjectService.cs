using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Helpers;

namespace WebApi.Services.Concrete
{
    public class SubjectService : ISubjectService
    {
        private DataContext _dbContext;
        private UnitOfWork _uow;
        private IMapper _mapper;
        private IGenericRepository<Subject> _repository;
        public SubjectService(DataContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _uow = new UnitOfWork(_dbContext);
            _repository = _uow.GetRepository<Subject>();
            _mapper = mapper;
        }

        public IEnumerable<Subject> All => _repository.GetAll();

        public SubjectDTO Create(SubjectDTO dto)
        {
            try
            {
                var _entity = _mapper.Map<Subject>(dto);
                _entity.Parent = _repository.Get(dto.ParentId);
                _repository.Add(_entity);

                _uow.SaveChanges();
                var _dto = _mapper.Map<SubjectDTO>(_entity); 
                return _dto;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public SubjectDTO SoftDelete(int id)
        {
            try
            {
                var entity = _repository.Get(id);
                _repository.Delete(entity); 
                _uow.SaveChanges();
                return _mapper.Map<SubjectDTO>(entity);
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        public SubjectDTO GetById(int id)
        {
            try
            {
                var _source = _repository.FindBy(p => p.Id == id)
                    .Include(p => p.Children)
                    .Include(p => p.Parent)
                    .FirstOrDefault();
                    
                var _dto = _mapper.Map<SubjectDTO>(_source);
                return _dto;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<SubjectDTO>> GetAllSubjects()
        {
            try
            {
                var _data = await _repository.GetAll()
                    .Include(p => p.Parent)
                    .Include(p => p.Children)
                    .ToListAsync();

                return _mapper.Map<List<SubjectDTO>>(_data).OrderByDescending(p => p.Id);
            }
            catch (System.Exception)
            {

                throw;
            }
        }



        public SubjectDTO Update(SubjectDTO dto)
        {
            try
            {
                var entity = _repository.Get(dto.Id);
                entity.Code = dto.Code;
                entity.Name = dto.Name;
                entity.Desc = dto.Desc;
                entity.Parent = _repository.Get(dto.ParentId);
                _repository.Update(entity, dto.Id);
                _uow.SaveChanges();
                return _mapper.Map<SubjectDTO>(entity);
            }
            catch (System.Exception)
            {

                throw;
            }
        }
    }
}