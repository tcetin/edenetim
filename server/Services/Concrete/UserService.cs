using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using WebApi.Data;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Helpers;


namespace WebApi.Services.Concrete
{

    public class UserService : IUserService
    {
        private DataContext _dbContext;
        private UnitOfWork _uow;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;
        private IGenericRepository<User> _repository;

        public UserService(DataContext dbContext, IMapper mapper,IOptions<AppSettings> appSettings)
        {
            _dbContext = dbContext;
            _uow = new UnitOfWork(_dbContext);
            _repository = _uow.GetRepository<User>();
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        public UserDTO Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = _repository.FindBy(p=>p.Username==username).FirstOrDefault();

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            var _dto = _mapper.Map<UserDTO>(user);
            _dto.Token = tokenString;

            // authentication successful
            return _dto;
        }

        public async Task<IEnumerable<UserDTO>> GetAllUsers()
        {
            try
            {
                var _data = await _repository.GetAll().ToListAsync();

                return _mapper.Map<List<UserDTO>>(_data).OrderByDescending(p => p.Id);
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        public UserDTO GetById(int id)
        {
            var _user =  _repository.Get(id);
            return _mapper.Map<UserDTO>(_user);
        }

        public UserDTO Create(UserDTO _user)
        {
            string password = _user.Username;
            var user =_mapper.Map<User>(_user);
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            if (_repository.FindBy(p => p.Username == user.Username).Any())
                throw new AppException("'" + user.Username + "' kullanıcısı sistemde zaten kayıtlı");

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            _repository.Add(user);
            _uow.SaveChanges();

            return _mapper.Map<UserDTO>(user);
        }

        public UserDTO Update(UserDTO userParam)
        {
            var user = _repository.Get(userParam.Id);

            if (user == null)
                throw new AppException("Kullanıcı bulunamadı!");

            if (userParam.Username != user.Username)
            {
                // username has changed so check if the new username is already taken
                if (_repository.FindBy(p => p.Username == userParam.Username).Any())
                    throw new AppException(userParam.Username + " kullanıcı adı ile sistemde kullanıcı kaydı mevcut.");
            }

            // update user properties
            user.FirstName = userParam.FirstName;
            user.LastName = userParam.LastName;
            user.Username = userParam.Username;
            user.Phone = userParam.Phone;
            user.Email = userParam.Email;
            // update password if it was entered
            if (!string.IsNullOrWhiteSpace(userParam.Password))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(userParam.Password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            _repository.Update(user,user.Id);
            _uow.SaveChanges();

            

            return _mapper.Map<UserDTO>(user);
        }

        public UserDTO Delete(int id)
        {
            var user = _repository.Get(id);

            _repository.Delete(user);
            _uow.SaveChanges();

            return _mapper.Map<UserDTO>(user);
        }

        public async Task<UserDTO> UploadAvatar(int id, IFormFile file)
        {
            try
            {
                var user = _repository.Get(id);
                using (var memoryStream = new MemoryStream())
                {
                    await file.CopyToAsync(memoryStream);
                    user.Photo = memoryStream.ToArray();
                }

                _repository.Update(user, user.Id);
                _uow.SaveChanges();
                
                return _mapper.Map<UserDTO>(user);
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        // private helper methods

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Değer zorunludur ve boşluk içeremez.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Değer zorunludur ve boşluk içeremez.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
    }
}