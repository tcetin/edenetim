using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Helpers;

namespace WebApi.Services.Concrete
{
    public class ComplaintSourceService : IComplaintSourceService
    {
        private DataContext _dbContext;
        private UnitOfWork _uow;
        private IMapper _mapper;
        private IGenericRepository<ComplaintSource> _repository;
        public ComplaintSourceService(DataContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _uow = new UnitOfWork(_dbContext);
            _repository = _uow.GetRepository<ComplaintSource>();
            _mapper = mapper;
        }

        public IEnumerable<ComplaintSource> All => _repository.GetAll();

        public ComplaintSourceDTO Create(ComplaintSourceDTO dto)
        {
            try
            {
                var _entity = _mapper.Map<ComplaintSource>(dto);
                _repository.Add(_entity);
                _uow.SaveChanges();
                var _dto = _mapper.Map<ComplaintSourceDTO>(_entity);
                return _dto;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public ComplaintSourceDTO SoftDelete(int id)
        {
            try
            {
                var entity = _repository.Get(id);
                entity.IsDeleted = true;
                _repository.Update(entity, id);
                _uow.SaveChanges();
                return _mapper.Map<ComplaintSourceDTO>(entity);
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        public ComplaintSourceDTO GetById(int id)
        {
            try
            {
                var _source = _repository.Get(id);
                var _dto = _mapper.Map<ComplaintSourceDTO>(_source);
                return _dto;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<ComplaintSourceDTO>> GetAllComplaints()
        {
            try
            {
                var _data = await _repository.FindBy(x => x.IsDeleted == false).ToListAsync();

                return _mapper.Map<List<ComplaintSourceDTO>>(_data).OrderByDescending(p => p.Id);
            }
            catch (System.Exception)
            {

                throw;
            }
        }



        public ComplaintSourceDTO Update(ComplaintSourceDTO dto)
        {
            try
            {
                var entity = _repository.Get(dto.Id);
                entity.Name = dto.Name;
                entity.Desc = dto.Desc;
                entity.UpdatedDate = DateTime.Now;
                _repository.Update(entity, dto.Id);
                _uow.SaveChanges();
                return _mapper.Map<ComplaintSourceDTO>(entity);
            }
            catch (System.Exception)
            {

                throw;
            }
        }
    }
}