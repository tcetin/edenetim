
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface ISubjectService
    {
        IEnumerable<Subject> All { get; }
        SubjectDTO GetById(int id);
        SubjectDTO Create(SubjectDTO dto);
        SubjectDTO Update(SubjectDTO dto);
        SubjectDTO SoftDelete(int id);
        Task<IEnumerable<SubjectDTO>> GetAllSubjects();
    }
}