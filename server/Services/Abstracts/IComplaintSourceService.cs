
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IComplaintSourceService
    {
        IEnumerable<ComplaintSource> All { get; }
        ComplaintSourceDTO GetById(int id);
        ComplaintSourceDTO Create(ComplaintSourceDTO dto);
        ComplaintSourceDTO Update(ComplaintSourceDTO dto);
        void Delete(int id);
        ComplaintSourceDTO SoftDelete(int id);
        Task<IEnumerable<ComplaintSourceDTO>> GetAllComplaints();
    }
}