using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WebApi.Dtos;

namespace WebApi.Services
{
    public interface IUserService
    {
        UserDTO Authenticate(string username, string password);
        Task<IEnumerable<UserDTO>> GetAllUsers();
        UserDTO GetById(int id);
        UserDTO Create(UserDTO user);
        UserDTO Update(UserDTO user);
        UserDTO Delete(int id);
        Task<UserDTO> UploadAvatar(int id,IFormFile file);
    }
}