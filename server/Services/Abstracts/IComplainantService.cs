
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IComplainantService
    {
        IEnumerable<Complainant> All { get; }
        ComplainantDTO GetById(int id);
        ComplainantDTO Create(ComplainantDTO dto);
        ComplainantDTO Update(ComplainantDTO dto);
        ComplainantDTO SoftDelete(int id);
        Task<IEnumerable<ComplainantDTO>> GetAllComplainants();
    }
}