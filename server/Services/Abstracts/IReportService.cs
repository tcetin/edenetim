using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IReportService
    {
        AdminReportDTO GetAdminReport();
    }
}