
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Data.Entities.Models;
using WebApi.Dtos;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface ITaskTypeService
    {
        IEnumerable<TaskType> All { get; }
        TaskTypeDTO GetById(int id);
        TaskTypeDTO Create(TaskTypeDTO dto);
        TaskTypeDTO Update(TaskTypeDTO dto);
        TaskTypeDTO SoftDelete(int id);
        Task<IEnumerable<TaskTypeDTO>> GetAllTaskTypes();
    }
}