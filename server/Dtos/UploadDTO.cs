using System.Collections.Generic;
using WebApi.Helpers;

namespace WebApi.Dtos
{
    public class UploadDTO
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }
        public string Path { get; set; }
    }
}