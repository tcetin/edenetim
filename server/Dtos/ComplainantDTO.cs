using System;
using GraphQL.Types;
using WebApi.Data.Entities.Models;
using WebApi.Helpers;

namespace WebApi.Dtos
{
    public class ComplainantDTO : AuditableDTO
    {
        public string Name { get; set; }
        public string Desc { get; set; }
    }
}