using Microsoft.AspNetCore.Http;
using WebApi.Data.Entities.Models;

namespace WebApi.Dtos
{
    public class UserDTO:AuditableDTO
    
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public byte[] Photo { get; set; }
    }
}