using System;
using GraphQL.Types;
using WebApi.Data.Entities.Models;
using WebApi.Helpers;

namespace WebApi.Dtos
{
    public class AdminReportDTO
    {
        public int ComplaintCount{get;set;}
        public int ComplaintSourceCount { get; set; }
        public int SubjectParentCount { get; set; }
        public int SubjectChildCount { get; set; }
        public int TaskTypeCount { get; set; }
    }
}