using System.Collections.Generic;
using WebApi.Helpers;

namespace WebApi.Dtos
{
    public class PagingDTO
    {
        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public int RowCount { get; set; }
    }
}