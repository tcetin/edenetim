using System;
using System.Collections.Generic;
using GraphQL.Types;
using WebApi.Data.Entities.Models;
using WebApi.Helpers;

namespace WebApi.Dtos
{
    public class SubjectDTO : AuditableDTO
    {
        public int ParentId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public virtual Subject Parent { get; set; }
        public virtual IList<Subject> Children { get; set; }

    }

}