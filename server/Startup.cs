﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using WebApi.Helpers;
using WebApi.Services;
using WebApi.Services.Concrete;
using AutoMapper;
using Microsoft.IdentityModel.Tokens; 
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;
using GraphQL;
using GraphQL.Types;
using WebApi.Data.Entities;
using WebApi.Data.Entities.Graphql.GraphQueryTypes;
using WebApi.Data.Entities.Graphql;
using WebApi.Data;
using WebApi.Data.Entities.Graphql.InputTypes;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region postgres

            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddEntityFrameworkNpgsql().AddDbContext<DataContext>(options => options.UseNpgsql(connectionString));

            #endregion

            #region  AutoMapperConfiguration
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });

            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);
            #endregion

            services.AddCors();
            // services.AddDbContext<DataContext>(x => x.UseInMemoryDatabase("TestDb"));
            services.AddMvc();

            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            // configure DI for application services
            services.AddSingleton<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IComplaintSourceService,ComplaintSourceService>();
            services.AddScoped<IComplainantService,ComplainantService>();
            services.AddScoped<ITaskTypeService,TaskTypeService>();
            services.AddScoped<ISubjectService,SubjectService>();
            services.AddScoped<IUserService,UserService>();
            services.AddScoped<IReportService,ReportService>();

            #region GraphQL
            services.AddScoped<RootQuery>(); //Root Graph Query
            services.AddScoped<RootMutation>();
            services.AddScoped<IDocumentExecuter, DocumentExecuter>();
            services.AddScoped<ComplaintSourceType>();
            services.AddScoped<CompliantSourceInputType>();
            services.AddScoped<ComplainantGraphType>();
            services.AddScoped<ComplainantInputType>();
            services.AddScoped<TaskTypeGraphType>();
            services.AddScoped<TaskTypeInputType>();
            services.AddScoped<SubjectGraphType>();
            services.AddScoped<SubjectInputType>();
            services.AddScoped<UserGraphType>();
            services.AddScoped<UserInputType>();
            services.AddScoped<AdminReportGraphType>();

            
            services.AddScoped<RelayResourceFilter>();

            var sp = services.BuildServiceProvider();
            services.AddScoped<ISchema>(_ => new GraphQLSchema(type => (GraphType)sp.GetService(type)) { Query = sp.GetService<RootQuery>() });
            #endregion





        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            // global cors policy
            app.UseCors(builder => builder.WithOrigins("http://localhost:5000/graphql")
            .AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod());

            app.UseAuthentication();

            app.UseMvc();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
        }
    }
}
