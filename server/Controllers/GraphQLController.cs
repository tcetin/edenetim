using System;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Http;
using GraphQL.Relay.Http;
using GraphQL.Types;
using Microsoft.AspNetCore.Mvc;
using WebApi.Data.Entities;
using WebApi.Data.Entities.Graphql;
using WebApi.Helpers;

namespace WebApi.Controllers
{
    [Route("graphql")]
    public class GraphQLController : Controller
    {
        
        private readonly IDocumentExecuter _documentExecuter;
        private readonly ISchema _schema;

        public GraphQLController(IDocumentExecuter documentExecuter, ISchema schema)
        {
            _documentExecuter = documentExecuter;
            _schema = schema;
        }
        
        [ServiceFilter(typeof(RelayResourceFilter))]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]GraphQLQuery query)
        {
            if (query == null) { throw new ArgumentNullException(nameof(query)); }

            var inputs = query.Variables.ToInputs();
            var queryToExecute = query.Query;
            var files = this.Request.HasFormContentType ? this.Request.Form.Files : null;
            

            var executionOptions = new ExecutionOptions 
            {
                Schema = _schema,
                Query = queryToExecute,
                Inputs = inputs,
                OperationName = query.OperationName
            };

            var result = await _documentExecuter.ExecuteAsync(executionOptions).ConfigureAwait(false);


            if (result.Errors?.Count > 0)
                return BadRequest(result);
            else
                return Ok(result);

        }
    }
}